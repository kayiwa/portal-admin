window.onload = function(){
    // these variables would presumably be initially set by the server.
    //this.pager_items_all = [] // total number of items in the application; only a subset are available on this page.
    //this.pager_selections = [] // items selected throughout the application

    setup = function() {
        // this function is run once at the start of this script, to initiate variables.

        // blank slate; otherwise, sometimes the back button messes things up
        $('.inline_checkbox').each( function() {
            $(this).prop('checked', false)
        })
    }

    update = function() {
        // do not change the pager_items_all or pager_selections array in this method!  This is a derivative method to update the view based on the model.

        for (var i=0; i<this.pager_selections.length; i++) {

            item = this.pager_selections[i]

            // GUI: check each checkbox in pager_selections
            var selector = '.inline_checkbox[value="' + item + '"]'
            var checkbox = document.querySelector(selector)
            if (checkbox) {
                checkbox.checked = true
            }
        }

        // update page variables
        $('#pager_items_all').html(pager_items_all.length)
        $('#pager_selections').html(pager_selections.length)
        $('#qty_selected_items_this_page').html(String(
            $('.inline_checkbox:checked').length
        ))
        $('#qty_selected_items_other_pages').html(String(
            pager_selections.length - $('.inline_checkbox:checked').length
        ))

        // update "select all on this page" checkbox
        if ($('.inline_checkbox').length === $('.inline_checkbox:checked').length) {
            $('#select_this_page_checkbox')
                .prop('checked', true)
                .prop('indeterminate', false)
        } else if ($('.inline_checkbox:checked').length > 0) {
            $('#select_this_page_checkbox')
                .prop('checked', false)
                .prop('indeterminate', true)
        } else {
            $('#select_this_page_checkbox')
                .prop('checked', false)
                .prop('indeterminate', false)
        }

        // update "select all on all pages" checkbox
        if (pager_items_all.length === pager_selections.length) {
            $('#select_all_pages_checkbox')
                .prop('checked', true)
                .prop('indeterminate', false)
        } else if (pager_selections.length > 0) {
            $('#select_all_pages_checkbox')
                .prop('checked', false)
                .prop('indeterminate', true)
        } else {
            $('#select_all_pages_checkbox')
                .prop('checked', false)
                .prop('indeterminate', false)
        }

        // update form fields for sending back to server
        //var pager_items_all_field = $('#pager_items_all_field')
        //pager_items_all_field.attr('value', this.pager_items_all)
        var pager_selections_field = $('#pager_selections_field')
        pager_selections_field.attr('value', this.pager_selections)
    }

    updateArrayFromCheckbox = function(checkbox) {
        // add this checkbox value to pager_selections
        arr = window.pager_selections
        var itemID = parseFloat(checkbox.attr('value'))
        var index = parseFloat(arr.indexOf(itemID))

        if ((checkbox.prop('checked')) && (index < 0)){
            arr.push(itemID)

        } else if ((!checkbox.prop('checked')) && (index >= 0)) {
            arr.splice(index, 1)
        }
    }

    matchInlineCheckboxesToMe = function(model_checkbox) {
        // make checked status of all inline checkboxes match the passed checkbox
        $('.inline_checkbox').each( function() {
            if (!model_checkbox.prop('checked')) {
                $(this).prop('checked', false)
            } else {
                $(this).prop('checked', true)
            }
            updateArrayFromCheckbox($(this))
        })
    }

    // make sure things are accurate to start
    setup()

    // update the GUI
    update()

    $('#select_all_pages_checkbox').click( function() {
        // populate or clear the pager_selections array

        if ($(this).prop('checked')) {
            $.extend(window.pager_selections, window.pager_items_all);
        } else {
            window.pager_selections = []
        }

        // match "select this page checkbox" to this checkbox
        if ($(this).prop('checked')) {
            $('#select_this_page_checkbox').prop('checked', true)
        } else {
            $('#select_this_page_checkbox').prop('checked', false)
        }

        // then match inline checkboxes to this checkbox
        matchInlineCheckboxesToMe($(this))

        // then update the rest of the GUI
        update()
    })

    $('#select_this_page_checkbox').click( function() {
        // match inline checkboxes to this checkbox
        matchInlineCheckboxesToMe($(this))

        // then update the rest of the GUI
        update()
    })

    $('.inline_checkbox').click( function() {
        // update the selected items array to match the id of this checkbox
        updateArrayFromCheckbox($(this))

        // then update the rest of the GUI
        update()
    })
}
