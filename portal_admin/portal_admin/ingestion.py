from xml.etree.ElementTree import tostring, Element, SubElement
from xml.dom import minidom

from django.conf import settings

from portal_admin import models as pa
from portal_admin import utils

from django.utils.module_loading import import_by_path

from django.db.models import Min
import datetime

def create_digobj(record, profile_id, context_id, location_id=None):
    """
    Given an XML record, a profile, and context; creates a digital
    object.  This top-level object does not have any ElementSource(s).
    """

    profile = pa.Profile.objects.get(id=profile_id)
    context = pa.Context.objects.get(id=context_id)
    location = pa.Location.objects.get(id=location_id) if location_id else None

    digobject = pa.DigObject.objects.create(
        ark=utils.generate_ark(),
        title=utils.assemble_value(record, profile_id, 'title'),
        profile=profile,
        digobject_form=profile.digobject_form,
        location=location)

    pa.DigObjectContext.objects.create(
        digobject=digobject,
        context=context,
        digobject_status=pa.DigObjectStatus.objects.get(status='Deposited'))

    return digobject.id

def import_values(digobj_id, profile_id, record):
    """
    Given an XML record, a profile, and a digital object;
    creates the set of ElementSource(s) and stores them
    as ImportedValue(s), thus describing the digital object.
    """

    profile = pa.Profile.objects.get(id=profile_id)
    digobj = pa.DigObject.objects.get(id=digobj_id)

    sources = pa.ElementSource.objects.filter(profile=profile)
    for s in sources:
        if s.default_text:
            val = s.default_text
            element, created = pa.ValueElement.objects.get_or_create(element=val)
            pa.ImportedValue.objects.create(value=element,
                                            digobject=digobj,
                                            dcxml_format=s.dcxml_format)
        else:
            try:
                if s.use_filename:
                    vals = [record.filename.replace(".xml", "")]
                else:
                    vals = utils.get_clean_values(record.record, s.xpath, s.instance_number, s.delimiter)

                for val in vals:
                    element, created = pa.ValueElement.objects.get_or_create(element=val)
                    pa.ImportedValue.objects.create(value=element,
                                                    digobject=digobj,
                                                    dcxml_format=s.dcxml_format)
            except utils.FieldNotFound:
                continue
            except utils.ValNotFound:
                continue

def create_files(digobj_id, profile_id, context_id, record):
    """
    Creates File objects, representing first the XML file, or
    metadata record, used to define the digital object;
    and then creating a set of File objects to represent
    any media attached to the record.
    """

    profile = pa.Profile.objects.get(id=profile_id)
    context = pa.Context.objects.get(id=context_id)
    digobj = pa.DigObject.objects.get(id=digobj_id)

    checksum = utils.get_checksum(tostring(record.record))
    pa.File.objects.create(
        filename_original=record.filename,
        filename_new="{}.xml".format(digobj.ark),
        digobject=digobj,
        checksum=checksum)
    fsources = pa.FileSource.objects.filter(profile=profile)
    content = [utils.get_content_pointer(record, f) for f in fsources]

    if profile.is_remote:
        files = [(utils.get_checksum(c.encode('utf-8')), c.split(".")[-1], c.split("/")[-1]) for c in content]
    else:
        fpaths = [utils.find_fpath(c, digobj.digobject_form._type, record.path) for c in content]
        files = []
        for fpath in fpaths:
            with open(fpath,'rb') as f:
                files.append((utils.get_checksum(f.read()),
                              fpath.split(".")[-1],
                              fpath.split("/")[-1]))

    for f in files:
        pa.File.objects.create(
            filename_original=f[2],
            filename_new="{}.{}".format(digobj.ark, f[1]),
            digobject=digobj,
            checksum=f[0])

    tclass = import_by_path(settings.IMPORT_TRANSFER_BACKEND)
    transfer = tclass()
    transfer.transfer(digobj, context.id, record, files)

def add_dcxml_element(root, element_name, text, purpose=None):
        x = SubElement(root, element_name)
        if purpose:
            x.attrib['purpose'] = purpose
        x.text = str(text)

def create_dc_xml(digobj_id, context_id):
    """
    Creates a dc.xml file, describing the digital object;
    based on the various information pulled via ElementSource(s)
    """

    digobj = pa.DigObject.objects.get(id=digobj_id)

    deposited = pa.DigObjectStatus.objects.get(status="Deposited")
    deposited_path = utils.digobj_path(digobj_id, context_id, deposited.id)

    root = Element('dc')

    add_dcxml_element(root, 'title', digobj.title, 'title')
    add_dcxml_element(root, 'identifier', digobj.ark, 'primary_identifier')

    for imported_value in pa.ImportedValue.objects.filter(digobject=digobj, dcxml_format__use_dcxml_element=True):
        add_dcxml_element(root,
                          imported_value.dcxml_format.element_name,
                          imported_value.value.element,
                          imported_value.dcxml_format.dcxml_purpose)

    add_dcxml_element(root, 'publisher', digobj.profile.institution.full_name, 'institution_name')
    add_dcxml_element(root, 'publisher', digobj.profile.institution.url, 'institution_url')
    add_dcxml_element(root, 'publisher', digobj.location.pk, 'location')
    add_dcxml_element(root, 'type', digobj.digobject_form.dcxml_value, 'types')
    add_dcxml_element(root, 'format', digobj.digobject_form._format, 'original_metadata_format')

    for dfv in pa.DigObjectFacetValue.objects.filter(digobject=digobj,
                                                     facet_value__facet_group__context__id=context_id):
        dformat = dfv.facet_value.facet_group.dcxml_format
        value = dfv.facet_value.value
        add_dcxml_element(root, dformat.element_name, value, dformat.dcxml_purpose)
        add_dcxml_element(root, dformat.element_name,
                          "{}::{}".format(value[:1].upper(), value),
                          dformat.dcxml_purpose + "_alpha")

    for c in digobj.parent_set.all():
        add_dcxml_element(root, c.dcxml_format.element_name, c.parent.ark, c.dcxml_format.dcxml_purpose)

    for d in pa.DigObjectForm.objects.filter(pk__in=digobj.child_set.values_list('child__digobject_form', flat=True).distinct()):        
        add_dcxml_element(root, 'relation', "{}:{}".format(d._type, d._format), 'hasPart')

    date_published = digobj.event_set.filter(event_type__name="Status Change: Published")\
                                     .aggregate(m=Min('meta_event__datestamp'))
    # if there is a published object use that date else use now because we won't add the status
    # change event until after indexing. EV 8/26/2015
    d = date_published['m'].date() if date_published['m'] else datetime.date.today()
    add_dcxml_element(root, 'date', d, 'date_published')

    utils.ensure_dirs(deposited_path)

    filename = deposited_path.format('data') + "{}.dc.xml".format(digobj.ark)
    dc_xml_file = open(filename, 'w', encoding="utf-8")
    data = minidom.parseString(tostring(root, 'utf-8')).toprettyxml(indent="\t")
    dc_xml_file.write(data)
    dc_xml_file.close()
    return filename

def create_location_dcxml(location_id):
    location = pa.Location.objects.get(pk=location_id)
    root = Element('dc')

    add_dcxml_element(root, 'publisher', location.name, 'location_name')
    add_dcxml_element(root, 'publisher', location.url, 'location_url')
    add_dcxml_element(root, 'publisher', location.phone, 'location_phone')
    add_dcxml_element(root, 'publisher', location.email, 'location_email')
    add_dcxml_element(root, 'publisher', location.web_contact_form, 'location_web_contact_form')
    add_dcxml_element(root, 'publisher', location.institution.full_name, 'location_institution_name')
    add_dcxml_element(root, 'publisher', location.street_address, 'location_street_address')
    add_dcxml_element(root, 'publisher', location.city, 'location_city')
    add_dcxml_element(root, 'publisher', location.state, 'location_state')
    add_dcxml_element(root, 'publisher', location.country, 'location_country')
    add_dcxml_element(root, 'publisher', location.zipcode, 'location_zipcode')

    filename = utils.location_path(location.institution.xtf_name) + str(location.pk) + ".dc.xml"
    with open(filename, 'w', encoding="utf-8") as f:
        data = minidom.parseString(tostring(root, 'utf-8')).toprettyxml(indent="\t")
        f.write(data)
    return filename

def create_institution_dcxml(institution_id):
    inst = pa.Institution.objects.get(pk=institution_id)
    root = Element('dc')

    add_dcxml_element(root, 'full_name', inst.full_name)
    add_dcxml_element(root, 'facet_name', inst.facet_name)
    add_dcxml_element(root, 'xtf_name', inst.xtf_name)
    for l in inst.location_set.all():
        add_dcxml_element(root, 'location', l.pk)

    filename = utils.brand_path(inst.xtf_name) + inst.xtf_name + ".dc.xml"
    with open(filename, 'w', encoding="utf-8") as f:
        data = minidom.parseString(tostring(root, 'utf-8')).toprettyxml(indent="\t")
        f.write(data)
    return filename


def validate_xpath(record, xpath, instance, delimiter):
    values = []
    for v in utils.get_clean_values(record, xpath, instance, delimiter):
        values.append(v)
    return values
