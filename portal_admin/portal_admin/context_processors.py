from django.contrib.auth.models import User
from .models import Context, Institution

page_titles = {
    'overview': 'Overview',
    'add_tags': 'Add Tags',
    'admin_reset': 'Admin Reset',
    'batch_add_tags': 'Batch Add Tags',
    'browse': 'Browse',
    'browse_records': 'Browse Records',
    'browse_values': 'Browse Values',
    'create_profile': 'Browse Profile',
    'delete_error_log': 'Delete Error Log',
    'delete_record': 'Delete Record',
    'delete_records': 'Delete Records',
    'delete_tag': 'Delete Tag',
    'deposit': 'Deposit',
    'error_log': 'Error Log',
    'error_logs': 'Error Logs',
    'facet_values': 'Facet Values',
    'help': 'Help',
    'help_sent': 'Help Sent',
    'login': 'Login',
    'logout': 'Logout',
    'overview': 'Overview',
    'profile_saved': 'Profile Saved',
    'record': 'Record',
    'records_deleted': 'Records Deleted',
    'retag': 'Retag',
    'review_tags': 'Review Tags',
    'select_context': 'Select Context',
    'tag': 'Tag',
    'test_tool': 'Test Tool',
    'upload': 'Upload',
    'uploaded': 'Uploaded',
    'validate_filesource': 'Validate File Source',
    'validate_separator': 'Validate Separator',
    'validate_xpath': 'Validate XPath',
    }

def general_template_vars(request):
    path = request.get_full_path()
    url_base = path.rsplit('/')[1:][0]

    if url_base in page_titles:
        page_title = 'Metadata Hopper | {}'.format(page_titles[url_base])
    else:
        page_title = 'Metadata Hopper'

    user = request.user

    if 'context' in request.session:
        context = Context.objects.get(id=request.session['context'])
    else:
        context = ''

    if 'institution' in request.session:
        institution = Institution.objects.get(id=request.session['institution'])
    else:
        institution = ''

    return {
        'user': user,
        'context': context,
        'institution': institution,
        'page_title': page_title,
        'url_base': url_base,
        }
