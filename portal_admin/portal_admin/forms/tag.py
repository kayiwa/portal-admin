from django import forms

from django.forms.formsets import BaseFormSet
from django.forms.formsets import formset_factory

from portal_admin.models import (FacetValue,
                                 DigObject,
                                 DigObjectFacetValue,
                                 Institution,
                                 ImportedValue,
                                 TriggerTerm,)

from portal_admin.forms.base import BootstrapForm

class FacetValuesForm(BootstrapForm):
    values = forms.ModelMultipleChoiceField(queryset=FacetValue.objects.none(),
                                            widget=forms.CheckboxSelectMultiple,
                                            error_messages={'required': "Please select records",
                                                            'invalid': "Please select valid records"},
                                            required=True)
    action = forms.ChoiceField(choices=(("", "Select an action"),
                                        ("delete", "Delete selected tags from all records"),
                                        ("review", "Review selected tags"),),
                               error_messages={'required': "Please select an action",
                                               'invalid': "Please select a valid action"},
                               required=True)
    def __init__(self, *args, **kwargs):
        values = kwargs.pop('values', False)
        counts = kwargs.pop('counts', False)

        super(FacetValuesForm, self).__init__(*args, **kwargs)

        if values:
            self.fields['values'].queryset = values

        items = []
        for choice in self.fields['values'].choices:
            value = FacetValue.objects.get(pk=choice[0])
            items.append((value.pk, {'name': value.value,
                                     'count': counts[value.pk],
                                     'url': '#' }))
        self.fields['values'].widget.choices = items

class DigObjectFacetValueForm(BootstrapForm):
    values = forms.MultipleChoiceField(choices=[],
                                       widget=forms.CheckboxSelectMultiple,
                                       error_messages={'required': "Please select records",
                                                       'invalid': "Please select valid records"},
                                       required=True)
    def __init__(self, *args, **kwargs):
        values = kwargs.pop('values', False)
        self.pages = kwargs.pop('pages')
        self.page = self.pages.page(kwargs.pop('page'))
        super(DigObjectFacetValueForm, self).__init__(*args, **kwargs)
        self.fields['values'].choices = [(obj.pk, obj,) for obj in self.page.object_list]

class FacetForm(BootstrapForm):
    values = forms.ModelMultipleChoiceField(queryset=FacetValue.objects.none(),
                                            widget=forms.CheckboxSelectMultiple)

    # for this to work with multiple contexts, this form will need to accept
    # a request object, and limit it's vals and objs accordingly.  I'm not
    # sure yet how to do it.
    # The object_count method works fine, as far as I know, but it is slow.  
    # I also turned it off in the interface.  Trying to maximize speed.  AJB 2015-06-11
    def obj_count(self, val, group):
        dig_object_facet_values = DigObjectFacetValue.objects.filter(facet_value=val)

        institution = Institution.objects.filter(institutioncontext__context__facetgroup=group)

        objs = DigObject.objects.filter(profile__institution=institution,
                                        digobjectcontext__context=group.context,
                                        digobjectfacetvalue=dig_object_facet_values)
        count = objs.count()
        return count

    def __init__(self, *args, **kwargs):
        self.group = kwargs.pop('group', False)

        super(FacetForm, self).__init__(*args, **kwargs)

        if self.group:
            self.fields['values'].queryset = self.group.facetvalue_set.all()

        vals = self.fields['values'].queryset
        self.fields['values'].choices = []
        choices = self.fields['values'].choices

        for val in vals:
            item = (val.pk, val.value,)
            choices.append(item)

class BaseFacetGroupFormSet(BaseFormSet):
    '''
    A set of forms for tagging; in this case, the set of tabs on the Add Tags page.
    (This class is populated with FacetForms with FacetGroupFormSet below)
    '''
    def __init__(self, *args, **kwargs):

        # one form (tab) for each FacetGroup
        # a group is a FacetGroup model object
        self.groups = kwargs.pop('groups', [])

        # turn off Names tab temporarily, until we figure out a better solution.  
        # Currently the names list is just too massive and slow for the templates.  TODO
        self.groups = self.groups.exclude(label='Names')

        # ?
        self.instance = kwargs.pop('instance', False)

        # number of empty forms (tabs) to create
        self.extra = len(self.groups)
        super(BaseFacetGroupFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, index, **kwargs):
        kwargs['group'] = self.groups[index]

        return super(BaseFacetGroupFormSet, self)._construct_form(
            index, **kwargs)

# populate BaseFacetGroupFormSet with FacetForms
FacetGroupFormSet = formset_factory(FacetForm, formset=BaseFacetGroupFormSet)

class HiddenImportedValuesForm(forms.Form):
    imported_values = forms.ModelMultipleChoiceField(queryset=ImportedValue.objects.all(),
                                                     widget=forms.MultipleHiddenInput)

class RetagForm(BootstrapForm):
    trigger_terms = forms.ModelMultipleChoiceField(queryset=TriggerTerm.objects.all(),
                                                   widget=forms.CheckboxSelectMultiple)
    institutions = forms.ModelMultipleChoiceField(queryset=Institution.objects.all(),
                                                  widget=forms.CheckboxSelectMultiple)

class TagFilterForm(BootstrapForm):
    assigned = forms.ChoiceField(choices=(("", "Assigned by"),
                                          (True, "Auto"),
                                          (False, "Manual")))
