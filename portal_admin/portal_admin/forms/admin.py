from django import forms

from portal_admin.forms.base import BootstrapForm

class DateForm(BootstrapForm):
    start_date = forms.DateField()
    end_date = forms.DateField()

class ContactForm(BootstrapForm):
    message = forms.CharField(required=True, widget=forms.Textarea,
                              label="Message")
