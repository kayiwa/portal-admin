from django import forms

from portal_admin.fields import ObjectChoiceField, FieldChoiceField
from portal_admin.models import (DCXMLFormat,
                                 DigObject,
                                 DigObjectStatus,
                                 DigObjectForm,
                                 ValueElement,
                                 Context)

from portal_admin.forms.base import BootstrapForm
from django.core.urlresolvers import reverse

from django.forms.formsets import BaseFormSet
from django.forms.formsets import formset_factory

class FileForm(BootstrapForm):
    upload = forms.FileField()
    def __init__(self, *args, **kwargs):
        self.file_obj = kwargs.pop('file_obj')
        super(FileForm, self).__init__(*args, **kwargs)

class BaseFileFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.digobject = kwargs.pop('instance')
        self.extra = self.digobject.file_set.count()
        super(BaseFileFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, index, **kwargs):
        kwargs['file_obj'] = self.digobject.file_set.all()[index]
        return super(BaseFileFormSet, self)._construct_form(index, **kwargs)

FileFormSet = formset_factory(FileForm, formset=BaseFileFormSet)

class HiddenDigObjectsForm(forms.Form):
    objs = forms.ModelMultipleChoiceField(queryset=DigObject.objects.all(),
                                          widget=forms.MultipleHiddenInput)

class DigObjectFilterForm(BootstrapForm):
    status = forms.ModelChoiceField(queryset=DigObjectStatus.objects.all(),
                                    required=False,
                                    empty_label="Select status")
    type = forms.ChoiceField(choices=([('', "Select type")] + \
                                      list(DigObjectForm.objects.values_list('_type', 'dcxml_value').distinct())),
                             required=False)

class ImportedValueFilterForm(BootstrapForm):
    initial = {'predicate': ('element__icontains', 'contains')}
    field = FieldChoiceField(DCXMLFormat.objects.exclude(field_label=""),
                             required=False,
                             empty_label="Search all fields")
    predicate = forms.ChoiceField(choices=[('element__istartswith', 'starts with'), \
                                           ('element__icontains', 'contains'),],
                                  widget=forms.RadioSelect,
                                  required=False)
    q = forms.CharField(required=False,
                        label="Find keywords in original metadata")

class RecordForm(BootstrapForm):
    items = forms.MultipleChoiceField(choices=[],
                                      widget=forms.CheckboxSelectMultiple,
                                      error_messages={'required': "Please select records",
                                                      'invalid': "Please select valid records"},
                                      required=True)
    def __init__(self, request, *args, **kwargs):
        self.pages = kwargs.pop('pages')
        self.page = self.pages.page(kwargs.pop('page'))
        super(RecordForm, self).__init__(*args, **kwargs)

        objs = self.page.object_list
        self.fields['items'].choices = []

        choices = self.fields['items'].choices
        context = Context.objects.get(pk = request.session['context'])
        for obj in objs:
            item = (obj.pk, {'name': obj.title,
                             'identifier': obj.local_identifier,
                             'description': obj.description,
                             'thumbnail': obj.thumb(),
                             'status': obj.get_status(context),
                             'url': reverse('record', args=[obj.pk]) })
            choices.append(item)

class SelectImportedValuesForm(BootstrapForm):
    items = forms.MultipleChoiceField(choices=[],
                                      widget=forms.CheckboxSelectMultiple,
                                      error_messages={'required': "Please select records",
                                                      'invalid': "Please select valid records"},
                                      required=True)
    action = forms.ChoiceField(choices=(("", "Select an action"),
                                        ("delete", "Delete selected records"),
                                        ("review", "Review selected records"),
                                        ("add", "Add tags to selected records"),),
                               error_messages={'required': "Please select an action",
                                               'invalid': "Please select a valid action"},
                               required=True)

    def __init__(self, *args, **kwargs):
        self.pages = kwargs.pop('pages')
        self.page = self.pages.page(kwargs.pop('page'))
        super(SelectImportedValuesForm, self).__init__(*args, **kwargs)

        # Don't touch the first item in the choice tuple!
        values = self.page.object_list
        self.fields['items'].choices = []
        choices = self.fields['items'].choices
        for v in values:
            ve = ValueElement.objects.get(pk=v['value'])
            item = (v['value'], {'name': ve.element,
                                 'count': v['count'],
                                 'url': ve.get_absolute_url() })
            choices.append(item)
