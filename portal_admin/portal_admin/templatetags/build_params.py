from django import template

register = template.Library()

@register.filter
def build_params(value, arg):
    params = value
    result = "?"
    for p in params:
        if not p == arg:
            result += "{}={}&".format(p, params[p])
    return result + arg + "="
