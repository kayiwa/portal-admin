from django.contrib import admin
from portal_admin import models


class ConnectionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'parent', 'child', 'dcxml_format', 'id')

class DCXMLFormatAdmin(admin.ModelAdmin):
    list_display = ('element_name', 'field_label', 'dcxml_purpose', 'id')

class DigObjectAdmin(admin.ModelAdmin):
    list_display = ('ark', 'title', 'digobject_form', 'profile', 'id')
    search_fields = ('ark', 'title', 'profile__name', 'id')
    list_filter = ('digobject_form', 'profile',)

class DigObjectContextAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'digobject', 'context', 'digobject_status', 'id')
    save_as = True

class DigObjectFacetValueAdmin(admin.ModelAdmin):
    list_display = ('digobject', 'facet_value', 'id')
    raw_id_fields = ('digobject', 'facet_value', 'trigger_term')
    search_fields = ("digobject__title", 'facet_value__value', 'id')

class DigObjectFormAdmin(admin.ModelAdmin):
    list_display = ('_format', '_type', 'use_manifest', 'manifest_format', 'use_media_folder', 'dcxml_value', 'can_be_parent', 'can_be_child', 'id')

class DigObjectStatusAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'id')

class ElementSourceAdmin(admin.ModelAdmin):
    list_display = ('profile', 'dcxml_format', 'xpath', 'instance_number', 'id')

class EventAdmin(admin.ModelAdmin):
    list_display = ('meta_event', 'event_type', 'digobject', 'id')

class EventTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'log_name', 'id')

class FacetGroupAdmin(admin.ModelAdmin):
    list_display = ('label', 'dcxml_format', 'context', 'id')

class FacetValueAdmin(admin.ModelAdmin):
    list_display = ('id', 'value', 'facet_group' )
    list_display_links = ['id']
    list_editable = ('value',)
    list_filter = ('facet_group',)
    search_fields = ("value",)

class TriggerTermAdmin(admin.ModelAdmin):
    list_display = ('id', 'term', 'facet_value', 'match_type')
    list_display_links = ['id']
    list_editable = ('term',)
    search_fields = ('term',)

class ImportedValueAdmin(admin.ModelAdmin):
    list_display = ('digobject', 'short_value', 'dcxml_format', 'pk')

class InstitutionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'xtf_name', 'id')

class MetaEventAdmin(admin.ModelAdmin):
    list_display = ('profile', 'context', 'user', 'error_log_url', 'id')

class ValueElementAdmin(admin.ModelAdmin):
    list_display = ('element', 'id')
    search_fields = ('element', 'id')

admin.site.register(models.Connection, ConnectionAdmin)
admin.site.register(models.Context)
admin.site.register(models.DCXMLFormat, DCXMLFormatAdmin)
admin.site.register(models.DeletedDigObject)
admin.site.register(models.DeletedImportedValue)
admin.site.register(models.DigObject, DigObjectAdmin)
admin.site.register(models.DigObjectContext, DigObjectContextAdmin)
admin.site.register(models.DigObjectFacetValue, DigObjectFacetValueAdmin)
admin.site.register(models.DigObjectForm, DigObjectFormAdmin)
admin.site.register(models.DigObjectStatus, DigObjectStatusAdmin)
admin.site.register(models.ElementSource, ElementSourceAdmin)
admin.site.register(models.Event, EventAdmin)
admin.site.register(models.EventType, EventTypeAdmin)
admin.site.register(models.FacetGroup, FacetGroupAdmin)
admin.site.register(models.FacetValue, FacetValueAdmin)
admin.site.register(models.File)
admin.site.register(models.FileSource)
admin.site.register(models.ImportedValue, ImportedValueAdmin)
admin.site.register(models.Institution, InstitutionAdmin)
admin.site.register(models.InstitutionContext)
admin.site.register(models.Location)
admin.site.register(models.MatchType)
admin.site.register(models.MetaEvent, MetaEventAdmin)
admin.site.register(models.Path)
admin.site.register(models.Profile)
admin.site.register(models.ProfileField)
admin.site.register(models.Role)
admin.site.register(models.TriggerTerm, TriggerTermAdmin)
admin.site.register(models.UserRole)
admin.site.register(models.ValueElement, ValueElementAdmin)
