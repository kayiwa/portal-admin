from django.contrib.auth.decorators import user_passes_test, login_required
from functools import wraps
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.contrib.auth.views import redirect_to_login

def session_required(view):
    @wraps(view)
    def _wrapped_view(request, *args, **kwargs):
        u = request.user
        if u.is_authenticated() and u.is_active and u.userrole_set.count() > 0:
            s = request.session
            if 'institution' in s and 'context' in s and \
               (u.userrole_set.filter(context=s['context'],
                                      institution=s['institution']).exists() or \
                u.userrole_set.filter(context=s['context'], role__rolename="manager").exists()):
                return view(request, *args, **kwargs)
            else:
                return redirect_to_login(request.path, reverse('select_context'))
        else:
            return redirect_to_login(request.path, reverse('login'))
    return _wrapped_view
