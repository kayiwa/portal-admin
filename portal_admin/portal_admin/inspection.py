import os, shutil, tempfile, subprocess
from xml.etree.ElementTree import tostring

from portal_admin.models import File, FileSource, EventType, Profile, ElementSource
from portal_admin import utils
from portal_admin import custom_logging as errlog


# Verification Routines : raise errors, halt processing
def verify_has_xml(fpath):
    '''Verifies that an XML file exists in fpath.'''
    files = os.listdir(fpath)
    for f in files:
        if f.endswith('xml'):
            return
    raise errlog.NoXMLFound

def verify_xml_structure(fpath):
    '''Accepts a filepath to an XML file, and verifies its structure via xmllint.'''
    proc = subprocess.Popen(['xmllint', '--noout', fpath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outstream, errstream = proc.communicate()
    if errstream and len(errstream):
        raise errlog.XMLStructureInvalid

def verify_no_duplicate(record, profile_id):
    profile = Profile.objects.get(id=profile_id)
    checksum = utils.get_checksum(tostring(record.record))
    duplicate_xml = File.objects.filter(digobject__profile=profile, checksum=checksum)
    if duplicate_xml and len(duplicate_xml):
        event_type = EventType.objects.get(name='Deposit Failed: Duplicate Records')
        msg = "Duplicate Record Found for {}".format(record.filename)
        record.messages.append( (event_type, msg) )
        return record

    try:
        fsources = FileSource.objects.filter(profile=profile)
        content = [utils.get_content_pointer(record, f) for f in fsources]

        if profile.is_remote:
            checksums = [utils.get_checksum(uri.encode('utf-8')) for uri in content]
        else:
            fpaths = [utils.find_fpath(fpattern, profile.digobject_form._type, record.path) for fpattern in content]
            checksums = []
            for fpath in fpaths:
                if fpath != None:
                    with open(fpath, 'rb') as f:
                        checksums.append(utils.get_checksum(f.read()))
        duplicates = [File.objects.filter(digobject__profile=profile, checksum=c) for c in checksums]
        duplicates = [d for dups in duplicates for d in dups]
        if duplicates:
            event_type = EventType.objects.get(name='Deposit Failed: Duplicate Records')
            msg = "Duplicate Record Found for {}".format(record.filename)
            record.messages.append( (event_type, msg) )
    # these exceptions have already been noted in previous stages
    # so just pass
    except utils.FieldNotFound:
        pass
    except utils.ValNotFound:
        pass
    return record

def verify_split_node(fpath, split_node):
    from xml.etree.ElementTree import parse
    with open(fpath, 'r', encoding="utf-8") as f:
        records = parse(f).findall(split_node)
    if len(records) <= 0:
        raise errlog.InvalidSplitNode

# Examination Routines : collect events, descriptive information, continue processing
def xml_fields(record, profile_id):
    profile = Profile.objects.get(id=profile_id)
    fields = [(x.xpath.strip(), x.instance_number, x.dcxml_format.field_label) for x in ElementSource.objects.filter(profile=profile, dcxml_format__is_required=True)]
    fields.extend([(x.xpath.strip(), x.instance_number, "File source") for x in FileSource.objects.filter(profile=profile) if not x.use_filename])

    for xpath, instance, name in fields:
        try:
            [v for v in utils.get_clean_values(record.record, xpath, instance=instance)]
        except utils.FieldNotFound:
            event_type = EventType.objects.get(name='Deposit Failed: Missing Required Elements')
            msg = "{} Not Found at {}".format(name, xpath)
            record.messages.append((event_type, msg))
        except utils.ValNotFound:
            event_type = EventType.objects.get(name='Deposit Failed: Missing Required Values')
            msg = "Value Not Found for {} at {}".format(name, xpath)
            record.messages.append( (event_type, msg) )
    return record

def content(record, profile_id):
    profile = Profile.objects.get(id=profile_id)
    fsources = FileSource.objects.filter(profile=profile)

    content = []
    for f in fsources:
        try:
            content.append(utils.get_content_pointer(record, f))
        except utils.FieldNotFound:
            pass  # xml_fields() will have noted this issue, keep going.
        except utils.ValNotFound:
            pass

    for pointer in content:
        if profile.is_remote:
            try:
                tempdir = tempfile.mkdtemp() + '/'
                fpath = utils.wget_content(pointer, profile.digobject_form._type, tempdir)
                fname = pointer.split("/")[-1]
                assert fpath.endswith(fname)
            except utils.WgetExtError:
                event_type = EventType.objects.get(name='Deposit Failed: Media URL Not Found')
                msg = "Remote Content Not Found, at URI {}".format(pointer)
                record.messages.append( (event_type, msg) )
            except utils.WgetHttpError:
                event_type = EventType.objects.get(name='Deposit Failed: Media URL Not Found')
                msg = "Remote Content Not Found, at URI {}".format(pointer)
                record.messages.append( (event_type, msg) )
            except utils.WgetNoContent:
                event_type = EventType.objects.get(name='Deposit Failed: Media URL Not Found')
                msg = "Remote Content Not Found, at URI {}".format(pointer)
                record.messages.append( (event_type, msg) )
            shutil.rmtree(tempdir)
        else:
            fname = utils.find_fpath(pointer, profile.digobject_form._type, record.path)
            if not fname:
                event_type = EventType.objects.get(name='Deposit Failed: Media File Not Found')
                msg = "Local Content Not Found, looking for {}".format(pointer)
                record.messages.append( (event_type, msg) )
    return record

def find_metadata(img, records, profile_id):
    profile = Profile.objects.get(id=profile_id)
    fsources = FileSource.objects.filter(profile=profile)
    index = img.rfind(".")
    img_pointer = img[:index] if index > -1 else img
    for r in records:
        for f in fsources:
            try:
                if utils.get_content_pointer(r, f) == img_pointer:
                    return True
            except utils.FieldNotFound:
                pass
            except utils.ValNotFound:
                pass
    return False
