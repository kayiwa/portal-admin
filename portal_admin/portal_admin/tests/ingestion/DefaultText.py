import os
from xml.etree.ElementTree import parse

from django.test import TestCase

from portal_admin import ingestion, utils
from portal_admin.context_managers import clean_room
from portal_admin.models import Context, DigObject, Profile, DCXMLFormat, ElementSource, ImportedValue
from portal_admin.processor import Record

class DefaultText(TestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        self.context = Context.objects.get(name='Chicago Collections Consortium')
        self.profile = Profile.objects.get(name='AIC EAD Finding Aids')
        sources = ElementSource.objects.filter(profile=self.profile)
        for s in sources:
            s.default_text = "DEFAULT_TEXT"
            s.save()
        self.test_dir = 'portal_admin/tests/test_data/aic/title_reconstruction/'
        xml_files = [x for x in os.listdir(self.test_dir) if x.endswith('.xml')]
        records = [parse(open(self.test_dir+f,'r')).getroot() for f in xml_files]
        assert len(records) == 1
        self.record = Record(records[0], xml_files[0], self.test_dir)

    def test_default_text(self):
        with clean_room(self.test_dir, self.profile.id) as _dir:
            digobj_id = ingestion.create_digobj(self.record, self.profile.id, self.context.id)
            ingestion.import_values(digobj_id, self.profile.id, self.record)
            digobj = DigObject.objects.get(id=digobj_id)
            do_values = ImportedValue.objects.filter(digobject=digobj)
            for v in do_values:
                assert v.value.element == 'DEFAULT_TEXT'



