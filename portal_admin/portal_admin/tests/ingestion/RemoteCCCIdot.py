from .IngestTestCase import IngestTestCase

from portal_admin.models import Context, Profile

class RemoteCCCIdot(IngestTestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        IngestTestCase.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id
        profile = Profile.objects.get(id=self.profile_id)
        profile.is_remote = True
        profile.save()

        self.test_dir = "portal_admin/tests/test_data/ccc_idot/remote_single_xml/"
        self.test_name = __name__


