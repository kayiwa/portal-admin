from django.core.urlresolvers import reverse

from selenium import webdriver

from .SeleniumTest import SeleniumTest

class Overview(SeleniumTest):
    '''
    The Contexts tests are to make sure the system responds correctly to different users, 
    institutions and contexts: whether the correct DigObjects, for example, show up in the 
    correct places within the interface.
    This Overview test is for the front page of the website.
    '''
    fixtures = ['initial_data.json',
                'test/test_ccc.json',
                'test/test_users.json',
                'test/test_digobjects.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url + reverse("login"))
        self.set_field("id_username", "kate")
        self.set_field("id_password", "admin")
        self.click("login")
        self.select_context("Chicago Collections Consortium", "University of Illinois at Chicago")

    def tearDown(self):
        self.browser.quit()

    # check if all main pages have the right titles
    def test_page_titles(self):
        titles = {
                'overview': 'Overview',
                'browse': 'Browse',
                'deposit': 'Deposit',
                'tag': 'Tag',
            }
        for key, value in titles.items():
            self.browser.get(self.live_server_url + reverse(key))
            actual_title = self.browser.title
            if value:
                assert 'Metadata Hopper | '.format(value) in actual_title
            else:
                assert actual_title == 'Metadata Hopper'

    def test_overview(self):
        '''
        Overview page should load and show a table of data.
        '''
        self.verify_body([
            "Overview",
            'University of Illinois at Chicago',
            'Metadata Hopper lets you deposit content to the Chicago Collections Consortium portal',
            'IDOT content dm images',
            'UIC MARCXML'])

    def test_quick_search(self):
        '''
        Quick search should return accurate values.
        '''
        quick_search = self.browser.find_element_by_css_selector("#quick_search")
        quick_search_submit = self.browser.find_element_by_css_selector("#quick_search_submit")
        quick_search.send_keys('Construction')
        quick_search_submit.click()
        self.verify_body(["1 record found", "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)"],['Traffic'])

class Context1(SeleniumTest):
    fixtures = ['initial_data.json',
                'test/test_ccc.json',
                'test/test_users.json',
                'test/Contexts.json',
                'test/test_digobjects.json',
                'test/DigObjectContexts.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.login("kate", "admin")
        self.verify_body(["Context", "Institution"])
        self.select_context("Chicago Collections Consortium", "Chicago Collections Consortium")

    def tearDown(self):
        self.browser.quit()

    def test_records_count(self):
        '''
        The overview table should show no records.
        '''
        self.browser.get(self.live_server_url + reverse('overview'))
        self.verify_body(["Chicago Collections Consortium", "Overview"])

        id_head_col0 = self.browser.find_element_by_id('id_head_col0')
        id_head_col1 = self.browser.find_element_by_id('id_head_col1')
        id_head_col2 = self.browser.find_element_by_id('id_head_col2')
        id_head_col3 = self.browser.find_element_by_id('id_head_col3')
        id_head_col4 = self.browser.find_element_by_id('id_head_col4')
        id_head_col5 = self.browser.find_element_by_id('id_head_col5')

        id_row1_col0 = self.browser.find_element_by_id('id_row1_col0')
        id_row1_col1 = self.browser.find_element_by_id('id_row1_col1')
        id_row1_col2 = self.browser.find_element_by_id('id_row1_col2')
        id_row1_col3 = self.browser.find_element_by_id('id_row1_col3')
        id_row1_col4 = self.browser.find_element_by_id('id_row1_col4')
        id_row1_col5 = self.browser.find_element_by_id('id_row1_col5')

        id_row2_col0 = self.browser.find_element_by_id('id_row2_col0')
        id_row2_col1 = self.browser.find_element_by_id('id_row2_col1')
        id_row2_col2 = self.browser.find_element_by_id('id_row2_col2')
        id_row2_col3 = self.browser.find_element_by_id('id_row2_col3')
        id_row2_col4 = self.browser.find_element_by_id('id_row2_col4')
        id_row2_col5 = self.browser.find_element_by_id('id_row2_col5')

        id_foot_col1 = self.browser.find_element_by_id('id_foot_col1')
        id_foot_col2 = self.browser.find_element_by_id('id_foot_col2')
        id_foot_col3 = self.browser.find_element_by_id('id_foot_col3')
        id_foot_col4 = self.browser.find_element_by_id('id_foot_col4')
        id_foot_col5 = self.browser.find_element_by_id('id_foot_col5')

        self.assertEqual(id_head_col0.text, 'Profile')
        self.assertEqual(id_head_col1.text, 'Deposited')
        self.assertEqual(id_head_col2.text, 'Previewed')
        self.assertEqual(id_head_col3.text, 'Published')
        self.assertEqual(id_head_col4.text, 'Other')
        self.assertEqual(id_head_col5.text, 'Total')

        self.assertEqual(id_row1_col0.text, 'Example EAD')
        self.assertEqual(id_row1_col1.text, '0')
        self.assertEqual(id_row1_col2.text, '0')
        self.assertEqual(id_row1_col3.text, '0')
        self.assertEqual(id_row1_col4.text, '0')
        self.assertEqual(id_row1_col5.text, '0')

        self.assertEqual(id_row2_col0.text, 'Example Image Collection')
        self.assertEqual(id_row2_col1.text, '0')
        self.assertEqual(id_row2_col2.text, '0')
        self.assertEqual(id_row2_col3.text, '0')
        self.assertEqual(id_row2_col4.text, '0')
        self.assertEqual(id_row2_col5.text, '0')

        self.assertEqual(id_foot_col1.text, '0')
        self.assertEqual(id_foot_col2.text, '0')
        self.assertEqual(id_foot_col3.text, '0')
        self.assertEqual(id_foot_col4.text, '0')
        self.assertEqual(id_foot_col5.text, '0')


class Context2(SeleniumTest):
    fixtures = ['initial_data.json',
                'test/test_ccc.json',
                'test/test_users.json',
                'test/test_digobjects.json',
                'test/DigObjectContexts.json',
                'test/Contexts.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.login("kate", "admin")
        self.verify_body(["Context", "Institution"])
        self.select_context("Chicago Collections Consortium", "University of Illinois at Chicago")

    def tearDown(self):
        self.browser.quit()

    def test_records_count(self):
        '''
        The overview table should show the correct number of records in the correct places within the overview table.
        '''
        self.browser.get(self.live_server_url + reverse('overview'))
        self.verify_body(["University of Illinois at Chicago", "Overview"])

        id_head_col0 = self.browser.find_element_by_id('id_head_col0')
        id_head_col1 = self.browser.find_element_by_id('id_head_col1')
        id_head_col2 = self.browser.find_element_by_id('id_head_col2')
        id_head_col3 = self.browser.find_element_by_id('id_head_col3')
        id_head_col4 = self.browser.find_element_by_id('id_head_col4')
        id_head_col5 = self.browser.find_element_by_id('id_head_col5')

        id_row1_col0 = self.browser.find_element_by_id('id_row1_col0')
        id_row1_col1 = self.browser.find_element_by_id('id_row1_col1')
        id_row1_col2 = self.browser.find_element_by_id('id_row1_col2')
        id_row1_col3 = self.browser.find_element_by_id('id_row1_col3')
        id_row1_col4 = self.browser.find_element_by_id('id_row1_col4')
        id_row1_col5 = self.browser.find_element_by_id('id_row1_col5')

        id_row2_col0 = self.browser.find_element_by_id('id_row2_col0')
        id_row2_col1 = self.browser.find_element_by_id('id_row2_col1')
        id_row2_col2 = self.browser.find_element_by_id('id_row2_col2')
        id_row2_col3 = self.browser.find_element_by_id('id_row2_col3')
        id_row2_col4 = self.browser.find_element_by_id('id_row2_col4')
        id_row2_col5 = self.browser.find_element_by_id('id_row2_col5')

        id_foot_col1 = self.browser.find_element_by_id('id_foot_col1')
        id_foot_col2 = self.browser.find_element_by_id('id_foot_col2')
        id_foot_col3 = self.browser.find_element_by_id('id_foot_col3')
        id_foot_col4 = self.browser.find_element_by_id('id_foot_col4')
        id_foot_col5 = self.browser.find_element_by_id('id_foot_col5')

        self.assertEqual(id_row1_col0.text, 'EAD')
        self.assertEqual(id_row1_col1.text, '0')
        self.assertEqual(id_row1_col2.text, '0')
        self.assertEqual(id_row1_col3.text, '0')
        self.assertEqual(id_row1_col4.text, '0')
        self.assertEqual(id_row1_col5.text, '0')

        self.assertEqual(id_row2_col0.text, 'IDOT content dm images')
        self.assertEqual(id_row2_col1.text, '8')
        self.assertEqual(id_row2_col2.text, '2')
        self.assertEqual(id_row2_col3.text, '0')
        self.assertEqual(id_row2_col4.text, '0')
        self.assertEqual(id_row2_col5.text, '10')

        self.assertEqual(id_foot_col1.text, '8')
        self.assertEqual(id_foot_col2.text, '2')
        self.assertEqual(id_foot_col3.text, '0')
        self.assertEqual(id_foot_col4.text, '0')
        self.assertEqual(id_foot_col5.text, '10')
