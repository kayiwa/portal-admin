from selenium import webdriver

from django.core.urlresolvers import reverse

from .SeleniumTest import SeleniumTest

from django.contrib.auth.models import User

class Login(SeleniumTest):
    fixtures = ['initial_data.json', 'test/test_ccc.json', 'test/test_users.json']

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def select_context(self, context, institution):
        self.select("id_context", context)
        self.select("id_institution", institution)
        self.click("login")

    def test_kate(self):
        self.login("kate", "admin")
        self.verify_body(["Context", "Institution"])
        self.select_context("Chicago Collections Consortium", "University of Chicago")
        self.verify_body(["University of Chicago", "Overview"])

    def test_archvist(self):
        self.login("archivist", "admin")
        self.verify_body(["University of Illinois at Chicago", "Overview"])

    def test_localadmin(self):
        self.login("localadmin", "admin")
        self.verify_body(["University of Illinois at Chicago", "Overview"])

    def test_next(self):
        self.browser.get(self.live_server_url + reverse("deposit"))
        self.verify_body(["Username", "Password"])
        self.set_field("id_username", "kate")
        self.set_field("id_password", "admin")
        self.click("login")
        self.select_context("Chicago Collections Consortium", "University of Chicago")
        self.verify_body(["Deposit & Update", "Digital Images", "Archival Collections"])

    def test_select_context(self):
        self.login("kate", "admin")
        self.browser.get(self.live_server_url + reverse("deposit"))
        self.verify_body(["Context:", "Institution:"])
        self.select_context("Chicago Collections Consortium", "University of Chicago")
        self.verify_body(["Deposit & Update", "Digital Images", "Archival Collections"])

    def test_no_roles(self):
        self.login("noroles", "admin")
        self.verify_body(["User is not associated with any institutions"])

    def test_deactivated(self):
        self.login("inactive_user", "admin")
        self.verify_body(["User's account has been deactivated"])

    def test_invalid(self):
        self.login("noone", "noone")
        self.verify_body(["Invalid login"])
