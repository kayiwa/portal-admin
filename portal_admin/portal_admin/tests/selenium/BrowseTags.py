from selenium import webdriver

from django.core.urlresolvers import reverse

from .SeleniumTest import SeleniumTest

class BrowseTags(SeleniumTest):
    fixtures = [
        'initial_data.json',
        'test/test_ccc.json',
        'test/test_users.json',
        'test/test_digobjects.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.login("kate", "admin")
        self.select_context(
            "Chicago Collections Consortium",
            "University of Illinois at Chicago")
        self.browser.get(self.live_server_url + reverse('browse'))
        self.click_link("By Tag Group")

    def tearDown(self):
        self.browser.quit()

    def test_filter_status(self):
        self.verify_body([
            "By Original Metadata",
            "By Tag Group",
            "Chicago Topics",
            "Cities"])
        self.select("id_status", "Deposited")
        self.click("id_limit")
        self.click_link("By Tag Group")
        self.verify_body([
            "By Original Metadata",
            "By Tag Group",
            "Chicago Topics",
            "Cities"])

    def test_filter_type1(self):
        self.verify_body([
            "By Original Metadata",
            "By Tag Group",
            "Chicago Topics",
            "Cities"])
        self.select("id_type", "Archival Collections")
        self.click("id_limit")
        self.click_link("By Tag Group")
        self.verify_body([], ["Chicago Topics"])

    def test_filter_type2(self):
        self.click_link("Chicago Topics")
        self.verify_body(["Transportation 10 records"])
        self.select("id_type", "Archival Collections")
        self.click("id_limit")
        self.verify_body([], ["Transportation 10 records"])

    def test_filter_next_page(self):
        self.select("id_status", "Deposited")
        self.click("id_limit")
        self.click_link("By Tag Group")
        self.click_link("Chicago Topics")
        # I think this is broken, but it may require some surgery.
        # Essentially, the limiters are lost when moving to the next page:
        #self.verify_body(["Transportation 9 records"])

    def test_next_page(self):
        self.click_link("Chicago Topics")
        self.verify_body(["Transportation 10 records"])

    def test_no_action(self):
        self.click_link("Chicago Topics")
        self.click("id_action_button_review")
        self.verify_body(["Review", "Untag", "Cancel"])

    def test_delete(self):
        self.click_link("Chicago Topics")
        self.click("id_values_0")
        self.click("id_action_button_review")

    def test_review(self):
        self.click_link("Chicago Topics")
        self.click("id_values_0") # Transportation
        self.click("id_action_button_review")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Traffic Intersection at Stockton Drive and LaSalle Street (image 06)",
            "Traffic Intersection at South Parkway and 51st Street (image 06)",
            "Traffic Intersection at Sheridan Road and Jonquil Terrace (image 02)",
            "Traffic Intersection at Michigan Blvd and Monroe (image 01)",
            "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)",
            "Traffic Intersection at Sheridan Road and Granville (image 06)",
            "Traffic Intersection at Sacramento Blvd and Van Buren (image 01)",
            "Traffic Intersection at Michigan Blvd and Oak Street (image 04)",
            "Traffic Intersection at South Parkway (East Drive) and 42nd Street"])
        self.click("id_items_0")
        self.click("id_action_button_delete")
        self.verify_body([
            "Are you sure you want to delete the following record completely from the Chicago Collections Consortium, including all related metadata and media files?",
            "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)"], 
                         ['Stockton Drive and LaSalle Street',
                          'Sheridan Road',
                         'Michigan Blvd and Monroe'])
        self.click("id_delete")
        self.verify_body([
            "The following record has been marked for deletion and will be deleted completely from the system including all related metadata and media files.",
            "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)"])

    def test_add_tags(self):
        self.browser.get(self.live_server_url + reverse('browse'))
        self.click_link("Geographic coverage")
        self.click("id_items_19") # Michigan Avenue (2 records)
        self.click("id_action_button_add")
        self.click("tag_pulldown_button")
        self.verify_body([
            "Traffic Intersection at Michigan Blvd and Monroe (image 01)",
            "Traffic Intersection at Michigan Blvd and Oak Street (image 04)"])

        # Select All checkbox
        checkboxes = [
            # Albany Park
            self.browser.find_element_by_id('id_form-0-values_0'),
            # Chicago Lawn
            self.browser.find_element_by_id('id_form-0-values_15'),
            # Forest Glen
            self.browser.find_element_by_id('id_form-0-values_24'),]
        for cb in checkboxes:
            self.assertFalse(cb.is_selected())
        self.click("id_checkbox_select_all_tab_1") # select all
        for cb in checkboxes:
            self.assertTrue(cb.is_selected())
        self.click("id_checkbox_select_all_tab_1") # deselect all
        for cb in checkboxes:
            self.assertFalse(cb.is_selected())

        # Filter tags box
        self.verify_body(["Albany Park", "Hyde Park", 'Logan Square', 'Pullman'])
        input_box_0 = self.browser.find_element_by_id('id_filter_tag_field_1')
        input_box_0.send_keys('Park')
        self.verify_body(
                   ["Albany Park", "Hyde Park"],
            not_in=['Logan Square', 'Pullman'])
        # Cities tab
        self.click('id_tag_tab_2')
        self.verify_body(
                   ["Arlington Heights", "Oak Park"],
            not_in=['Albany Park', 'Hyde Park'])
        input_box_1 = self.browser.find_element_by_id('id_filter_tag_field_2')
        input_box_1.send_keys('park')
        self.verify_body(
                   ["Oak Park"],
            not_in=['Arlington Heights'])
        # clear button on Cities tab
        self.click('id_filter_tag_button_2')
        # Arlington Heights
        self.click('id_form-1-values_0')
        self.verify_body(
                   ["Oak Park", 'Arlington Heights'])
        # Back to Neighborhoods tab
        self.click('id_tag_tab_1')
        # clear button on Neighborhoods tab
        self.click("id_filter_tag_button_1")
        self.verify_body( ["Albany Park", "Hyde Park", 'Logan Square', 'Pullman'])
        # Select Rogers Park
        self.click("id_form-0-values_61")
        self.click("id_submit")
        self.verify_body("Tags added")
        self.verify_body([
            'Neighborhoods Rogers Park added to:',
            'Traffic Intersection at Michigan Blvd and Monroe (image 01)',
            'Traffic Intersection at Michigan Blvd and Oak Street (image 04)',
            'Cities Arlington Heights added to:',
            'Traffic Intersection at Michigan Blvd and Monroe (image 01)',
            'Traffic Intersection at Michigan Blvd and Monroe (image 01)',
            ])

    def test_page_title(self):
        actual_title = self.browser.title
        assert 'Metadata Hopper | Browse' in actual_title

    def test_checkboxes(self):
        self.browser.get(self.live_server_url + reverse('browse'))
        self.click_link("Geographic coverage")
        self.click("select_this_page_checkbox")
        checkbox = self.browser.find_element_by_id('id_items_2')
        self.assertTrue(checkbox.is_selected())
        self.click("select_this_page_checkbox")
        self.assertFalse(checkbox.is_selected())

