import time

from selenium import webdriver
from django.test import TestCase, Client

from django.core.urlresolvers import reverse

from .SeleniumTest import SeleniumTest

from portal_admin.views.connect import ConnectConfirmView
from portal_admin.models import Connection, DigObject


class BrowseRecordsUnit(TestCase):
    fixtures = [
        'initial_data.json',
        'test/test_medium.json']

    def setUp(self):
        self.browse_url = '/records/'

        # login
        self.client = Client()
        self.login = self.client.post(
            '/accounts/login/', {'username': 'kate', 'password': 'admin'}, follow=True)

        # if no context, redirects to a form
        self.context_institution = self.client.post(
            '/accounts/selectcontext/', {'user': 2, 'context': 1, 'institution':14}, follow=True)

        self.record_names = [
            [ # page 1
            'Records <span class="lead">1 to 20</span> of 34 found',
            'Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago',
            'Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago',
            "Immigrants&#39; Protective League records: An inventory of the collection at the University of Illinois at Chicago"],
            [# page 2
            'Records <span class="lead">21 to 34</span> of 34 found',
            'Russell Ward Ballard collection: An inventory of the collection at the University of Illinois at Chicago',
            'Traffic Intersection at Sheridan Road and Jonquil Terrace (image 02)',
            'Wencel Hetman papers: An inventory of the collection at the University of Illinois at Chicago'
            ]
        ]

        self.page_1_response = self.client.get(self.browse_url)
        self.page_2_response = self.client.get(self.browse_url + '?page=2')

    def tearDown(self):
        pass

    def test_load_browse_url(self):
        '''After login in setUp, logins should happen without redirects, and  load browse page should also load without redirect.'''
        self.assertEqual(self.login.status_code, 200)
        self.assertEqual(self.context_institution.status_code, 200)

        self.assertEqual(self.page_1_response.status_code, 200)

    def test_names_exist(self):
        '''Each record name should exist on its respective page, but not in the other.'''

        for name in self.record_names[0]:
            self.assertContains(self.page_1_response, name)
            self.assertNotContains(self.page_2_response, name)

        for name in self.record_names[1]:
            self.assertContains(self.page_2_response, name)
            self.assertNotContains(self.page_1_response, name)


class BrowseRecordsSelenium(SeleniumTest):
    fixtures = ['initial_data.json', 'test/test_medium.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url + reverse("login"))
        self.set_field("id_username", "kate")
        self.set_field("id_password", "admin")
        self.click("login")
        self.select_context("Chicago Collections Consortium", "University of Illinois at Chicago")
        self.browser.get(self.live_server_url + reverse('browse'))

    def tearDown(self):
        self.browser.quit()

    def test_search(self):
        '''
        Searches should return accurate values.
        '''
        pass

    def test_filter_status(self):
        self.verify_body([
            "Title records",
            "Dates records",
            "Names records",
            "Subject records",
            "Source archival collection records",
            "Rights records",
            "Local identifier records",
            "Geographic coverage records"
            ])
        self.select("id_status", "Deposited")
        self.click("id_limit")
        self.verify_body([
            "Title records",
            "Dates records",
            "Names records",
            "Subject records",
            "Source archival collection records",
            "Rights records",
            "Local identifier records",
            "Geographic coverage records",
            ])

    def test_filter_type1(self):
        self.verify_body([
            "Title records",
            "Dates records",
            "Names records",
            "Subject records",
            "Source archival collection records",
            "Rights records",
            "Local identifier records",
            "Geographic coverage records"
            ])
        self.select("id_type", "Archival Collections")
        self.click("id_limit")
        self.verify_body([
            'By Original Metadata',
            'By Tag Group'])

    def test_filter_next_page(self):
        self.select("id_status", "Deposited")
        self.click("id_limit")
        self.click_link("Geographic coverage")
        self.verify_body(["Michigan Avenue",
                          "Jeffery Boulevard"],
                         ["Sheridan Road",
                          'South Parkway (East Drive)'])

    def test_next_page(self):
        self.click_link("Geographic coverage")
        self.verify_body([
            r"Michigan Avenue 2 records",
            r"Jeffery Boulevard 1 record"])

    def test_pagination(self):
        self.click_link("Geographic coverage")
        self.browser.find_element_by_css_selector(".page_next").click()
        self.verify_body(["South Parkway (East Drive) 1 record",
                          "Sheridan Road 2 records"])

# TODO temporarily deactivated because this feature needs work
#    def test_items_per_page(self):
#        self.click_link("Geographic coverage")
#        self.click_link("50")
#        self.verify_body(["Stockton Drive 1 record"])

    def test_filter_type2(self):
        self.click_link("Geographic coverage")
        self.verify_body([
            "Michigan Avenue 2 records",
            "Jeffery Boulevard 1 record"])
        self.select("id_type", "Archival Collections")
        self.click("id_limit")
        self.verify_body([], [
            "Michigan Avenue 2 records",
            "Jeffery Boulevard 1 record"])

    def test_filter_query(self):
        self.click_link("Geographic coverage")
        self.verify_body([
            "Michigan Avenue 2 records",
            "Jeffery Boulevard 1 record"])
        self.set_field("id_q", "4")
        self.click("id_predicate_1")

        predicate = self.browser.find_element_by_id("id_predicate_1")
        submit_button = self.browser.find_element_by_id("id_limit")

        submit_button.submit()
        self.verify_body(
            ["41.760565 1 record",
            "-87.654719 1 record",
            "Values 1 to 12 of 12 found"],
            ["Michigan Avenue"])
        predicate = self.browser.find_element_by_id("id_predicate_0")
        submit_button = self.browser.find_element_by_id("id_limit")
        predicate.click()
        submit_button.submit()

        # apparent selenium bug causes this to fail
        # self.verify_body(
        #    ["Values 1 to 9 of 9 found",
        #        "41.760565 1 record"],
        #    ["-87.654719", "Michigan Avenue"]
        #    )



    def test_no_action(self):
        self.click_link("Geographic coverage")
        self.click("id_action_button_add")
        self.verify_body("Please select records")

    def test_delete(self):
        self.click_link("Subject")
        self.verify_body(["Chicago Design History 5 records",
                          "Chicago Political and Civic Life 9 records"])
        self.click("id_items_13")
        self.click("id_action_button_delete")
        self.verify_body(["Nicolas J. Budinger papers: An inventory of the collection at the University of Illinois at Chicago",
                          "Jean Barr Cohen Theatre collection: An inventory of the collection at the University of Illinois at Chicago",
                          "Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago"])
        self.click("id_delete")
        self.verify_body(["Nicolas J. Budinger papers: An inventory of the collection at the University of Illinois at Chicago",
                          "Jean Barr Cohen Theatre collection: An inventory of the collection at the University of Illinois at Chicago",
                          "Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago"])

    def test_review(self):
        self.click_link("Geographic coverage")
        self.click("id_items_19") # Michigan Avenue
        self.click("id_action_button_review")
        self.verify_body([
            "Traffic Intersection at Michigan Blvd and Monroe (image 01)",
            "Traffic Intersection at Michigan Blvd and Oak Street (image 04)"])
        self.click("id_items_0")
        self.click("id_items_1")
        self.click("id_action_button_delete")
        self.verify_body([
            "Are you sure you want to delete the following records completely from the Chicago Collections Consortium, including all related metadata and media files?",
            "Traffic Intersection at Michigan Blvd and Monroe (image 01)",
            "Traffic Intersection at Michigan Blvd and Oak Street (image 04)"])
        self.click("id_delete")
        self.verify_body([
            "The following records have been marked for deletion and will be deleted completely from the system including all related metadata and media files.",
            "Traffic Intersection at Michigan Blvd and Monroe (image 01)",
            "Traffic Intersection at Michigan Blvd and Oak Street (image 04)"])

    def test_filter_review(self):
        self.click_link("Geographic coverage")
        self.click("id_items_18")
        self.click("id_items_19")
        self.click("id_action_button_review")
        self.verify_body([
            "Records 1 to 3 of 3 found",
            "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)",
            "Traffic Intersection at Michigan Blvd and Monroe (image 01)",
            "Traffic Intersection at Michigan Blvd and Oak Street (image 04)"])

    def test_browse_records_by_value(self):
        self.click_link("Subject")
        self.verify_body([
            "Values 1 to 20 of 83 found",
            "Chicago Political and Civic Life 9 records",
            "Child labor --United States --Sources. 1 record"])
        self.click("id_record_anchor_17")
        self.verify_body([
            "Records 1 to 9 of 9 found",
            "Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago",
            "Wencel Hetman papers: An inventory of the collection at the University of Illinois at Chicago"])

    def test_table_record_links(self):
        '''
        "No. of Records" links in table on Browse "By Original Metadata" should lead to correct records.
        '''
        self.verify_body([
            "Title records",
            "Source archival collection records",
            "Names records"])
        geo_coverage_link = self.browser.find_element_by_css_selector(
            '#id_record_link_15')
        geo_coverage_link.click()
        self.verify_body([
            'Records imported with the field label "Geographic coverage"',
            "Records 1 to 10 of 10 found"])

    def test_browse_records_select_all(self):
        '''Clicking "select all on all pages" should select all boxes in the table.'''
        self.browser.get(self.live_server_url + reverse('browse_records'))
        checkbox_select_all_pages = self.browser.find_element_by_css_selector(
            '#select_all_pages_checkbox')
        checkbox_select_all = self.browser.find_element_by_css_selector(
            '#select_this_page_checkbox')
        inline_checkboxes = self.browser.find_elements_by_css_selector(
            '.inline_checkbox')
        first_inline_checkbox = self.browser.find_element_by_css_selector(
            '#id_items_0')
        last_inline_checkbox = self.browser.find_element_by_css_selector(
            '#id_items_19')
        page_chevron_next = self.browser.find_element_by_css_selector(
            '.page_chevron_next')

        self.assertFalse(checkbox_select_all_pages.is_selected())
        self.assertFalse(checkbox_select_all.is_selected())
        for box in inline_checkboxes:
            self.assertFalse(box.is_selected())
        self.verify_body([
            'Records 1 to 20 of 34 found',
            '0 of 34 records selected',
            '(0 on this page, 0 on other pages)'
        ])


        checkbox_select_all_pages.click()
        self.assertTrue(checkbox_select_all_pages.is_selected())
        self.assertTrue(checkbox_select_all.is_selected())
        for box in inline_checkboxes:
            self.assertTrue(box.is_selected())
        self.verify_body([
            'Records 1 to 20 of 34 found',
            '34 of 34 records selected',
            '(20 on this page, 14 on other pages)'
        ])

        first_inline_checkbox.click()
        self.assertFalse(checkbox_select_all_pages.is_selected())
        self.assertFalse(checkbox_select_all.is_selected())
        self.assertFalse(first_inline_checkbox.is_selected())
        self.assertTrue(last_inline_checkbox.is_selected())
        self.verify_body([
            'Records 1 to 20 of 34 found',
            '33 of 34 records selected',
            '(19 on this page, 14 on other pages)'
        ])

        page_chevron_next.click()

        checkbox_select_all_pages = self.browser.find_element_by_css_selector(
            '#select_all_pages_checkbox')
        checkbox_select_all = self.browser.find_element_by_css_selector(
            '#select_this_page_checkbox')
        first_inline_checkbox = self.browser.find_element_by_css_selector(
            '#id_items_0')
        last_inline_checkbox = self.browser.find_element_by_css_selector(
            '#id_items_13')

        self.assertFalse(checkbox_select_all_pages.is_selected())
        self.assertTrue(checkbox_select_all.is_selected())
        self.assertTrue(first_inline_checkbox.is_selected())
        self.assertTrue(last_inline_checkbox.is_selected())
        self.verify_body([
            'Records 21 to 34 of 34 found',
            '33 of 34 records selected',
            '(14 on this page, 19 on other pages)'
        ])

        id_action_button_review = self.browser.find_element_by_css_selector(
            '#id_action_button_review')

        id_action_button_review.click()

        checkbox_select_all_pages = self.browser.find_element_by_css_selector(
            '#select_all_pages_checkbox')
        checkbox_select_all = self.browser.find_element_by_css_selector(
            '#select_this_page_checkbox')
        inline_checkboxes = self.browser.find_elements_by_css_selector(
            '.inline_checkbox')

        self.assertFalse(checkbox_select_all_pages.is_selected())
        self.assertFalse(checkbox_select_all.is_selected())
        for box in inline_checkboxes:
            self.assertFalse(box.is_selected())

        self.verify_body([
            'Records 1 to 20 of 33 found',
            '0 of 33 records selected',
            '(0 on this page, 0 on other pages)'
        ])
