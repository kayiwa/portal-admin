import time
from selenium import webdriver

from django.core.urlresolvers import reverse

from .SeleniumTest import SeleniumTest

class MultiPage(SeleniumTest):
    '''
    This test class is for testing elements of the interface which require larger quantities of data to make sense, like the paging functions.
    '''
    # installing the test_users.json file twice makes it work
    fixtures = ['initial_data.json', 'test/test_medium.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url + reverse("login"))
        self.set_field("id_username", "kate")
        self.set_field("id_password", "admin")
        self.click("login")
        self.select_context("Chicago Collections Consortium", "University of Illinois at Chicago")

    def tearDown(self):
        self.browser.quit()

    def test_pager_browse_records(self):
        self.browser.get(self.live_server_url + reverse('browse_records'))
        self.verify_source(["Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago",
                            "Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago",
                            'page_next',
                            'page_current'], 
                           ["Russell Ward Ballard collection: An inventory of the collection at the University of Illinois at Chicago",
                            "Wencel Hetman papers: An inventory of the collection at the University of Illinois at Chicago",
                            'page_prev'])

        page_chevron_prev = self.browser.find_element_by_css_selector(
            '.page_chevron_prev')
        page_chevron_next = self.browser.find_element_by_css_selector(
            '.page_chevron_next')
        page_current = self.browser.find_element_by_css_selector(
            '.page_current')
        page_next = self.browser.find_element_by_css_selector(
            '.page_next')
        self.assertIn(page_current.text, "1")
        self.assertIn(page_next.text, "2")

        page_next.click()

        self.verify_source(["Russell Ward Ballard collection: An inventory of the collection at the University of Illinois at Chicago",
                            "Wencel Hetman papers: An inventory of the collection at the University of Illinois at Chicago",
                            'page_prev',
                            'page_current'],
                           ["Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago",
                            "Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago",
                            'page_next'])

        page_prev = self.browser.find_element_by_css_selector(
            '.page_prev')
        page_current = self.browser.find_element_by_css_selector(
            '.page_current')
        self.assertIn(page_prev.text, "1")
        self.assertIn(page_current.text, "2")

    def test_pager_browse_metadata(self):
        self.browser.get(self.live_server_url + reverse('browse'))
        self.click_link("Title")

        self.verify_source(["Charles G. Dawes papers",
                            "Polish Falcons of America collection",
                            'page_current',
                            'page_next'],
                           ["Russell Ward Ballard collection",
                            "Wencel Hetman papers",
                            'page_prev'])

        page_chevron_prev = self.browser.find_element_by_css_selector(
            '.page_chevron_prev')
        page_chevron_next = self.browser.find_element_by_css_selector(
            '.page_chevron_next')
        page_current = self.browser.find_element_by_css_selector(
            '.page_current')
        page_next = self.browser.find_element_by_css_selector(
            '.page_next')
        self.assertIn(page_current.text, "1")
        self.assertIn(page_next.text, "2")

        page_next.click()

        self.verify_source(["Russell Ward Ballard collection",
                            "Wencel Hetman papers",
                            'page_current',
                            'page_prev'],
                           ["Charles G. Dawes papers",
                            "Polish Falcons of America collection",
                            'page_next'])

        page_prev = self.browser.find_element_by_css_selector(
            '.page_prev')
        page_current = self.browser.find_element_by_css_selector(
            '.page_current')
        self.assertIn(page_prev.text, "1")
        self.assertIn(page_current.text, "2")
