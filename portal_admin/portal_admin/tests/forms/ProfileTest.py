from django.test import TestCase

from portal_admin.models import DigObjectForm, DCXMLFormat, ProfileField, Profile, Institution

class ProfileTest(TestCase):
    def setUp(self):
        super(ProfileTest, self).setUp()
        ## These test objects are just needed to fill out the required fields
        ## The values don't affect the results of these tests
        ## except the dcxml_format.is_required - ev 6/2/2014
        self.digobject_form = DigObjectForm.objects.create(_format="format",
                                                           _type="type")

        self.dcxml_format = DCXMLFormat.objects.create(dcxml_purpose="purpose",
                                                       field_label="label",
                                                       element_name="element name",
                                                       is_required=True,
                                                       use_dcxml_element=False)

        institution = Institution.objects.create(full_name="Institution",
                                                 xtf_name="inst",
                                                 facet_name="Institution",
                                                 url="http://institution.edu")

        self.profile = Profile.objects.create(name="Profile",
                                              description="description",
                                              institution=institution,
                                              is_remote=False,
                                              split_node="node",
                                              digobject_form=self.digobject_form,
                                              use_metadata_filename=False)

    def create_profile_field(self, complx=False, delimiter=False, instance=False, instance_all=False, attribute=False, attribute_value=False, filename=False):
        return ProfileField.objects.create(digobject_form=self.digobject_form,
                                           dcxml_format=self.dcxml_format,
                                           use_source_widget=(not complx and not filename),
                                           source_label="Source element path",
                                           use_complex_widget=complx,
                                           use_instance_widget=instance,
                                           use_instance_all=instance_all,
                                           use_attribute_widget=attribute,
                                           use_attribute_value_widget=attribute_value,
                                           use_delimiter_widget=delimiter,
                                           use_filename_widget=filename,
                                           field_order=0)


