from .ProfileTest import ProfileTest

from portal_admin.forms.deposit import ElementSourceForm

class ElementSource(ProfileTest):

    def invalid_form(self, data, profile_field, num_errors):
        form = ElementSourceForm(data, profile_field=profile_field)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), num_errors)

    def valid_form(self, data, profile_field, num_fields, xpath, instance_number=-1, delimiter=None, default_text=None, use_filename=False):
        form = ElementSourceForm(data, profile_field=profile_field)
        self.assertEqual(len(form.fields), num_fields)
        self.assertTrue(form.is_valid())

        e = form.save(self.profile)

        self.assertEqual(e.xpath, xpath)
        self.assertEqual(e.dcxml_format, self.dcxml_format)
        self.assertEqual(e.instance_number, instance_number)
        self.assertEqual(e.delimiter, delimiter)
        self.assertEqual(e.default_text, default_text)
        self.assertEqual(e.use_filename, use_filename)

    def test_strip_whitespace_xpath(self):
        pf = self.create_profile_field()
        self.valid_form({'xpath': " /title/1 "}, pf, 1, "/title/1")

    def test_strip_slash_xpath(self):
        pf = self.create_profile_field()
        self.valid_form({'xpath': " /title/1/"}, pf, 1, "/title/1")

    def test_simple_xpath(self):
        pf = self.create_profile_field()
        self.valid_form({'xpath': "/title/1"}, pf, 1, '/title/1')

    def test_simple_xpath_invalid(self):
        pf = self.create_profile_field()
        self.invalid_form({}, pf, 1)

    def test_xpath_with_attribute(self):
        pf = self.create_profile_field(attribute=True)
        data = {'xpath': "/title/1", 'attribute': "date"}
        self.valid_form(data, pf, 2, "/title/1/@date")

    def test_strip_whitespace_attribute(self):
        pf = self.create_profile_field(attribute=True)
        data = {'xpath': "/title/1/ ", 'attribute': " date "}
        self.valid_form(data, pf, 2, "/title/1/@date")

    def test_xpath_with_attribute_invalid(self):
        pf = self.create_profile_field(attribute=True)
        self.invalid_form({}, pf, 1)

    def test_xpath_without_attribute(self):
        pf = self.create_profile_field(attribute=True)
        self.valid_form({'xpath': "/title/1"}, pf, 2, "/title/1")

    def test_xpath_with_attribute_value(self):
        pf = self.create_profile_field(attribute=True, attribute_value=True)
        data = {'xpath': "/title/1", 'attribute': "date", 'attribute_value': "12/12/12"}
        self.valid_form(data, pf, 3, "/title/1[@date=\"12/12/12\"]")

    def test_strip_whitespace_attribute_value(self):
        pf = self.create_profile_field(attribute=True, attribute_value=True)
        data = {'xpath': "/title/1", 'attribute': " date ", 'attribute_value': " 12/12/12 "}
        self.valid_form(data, pf, 3, "/title/1[@date=\"12/12/12\"]")

    def test_xpath_without_attribute_value(self):
        pf = self.create_profile_field(attribute=True, attribute_value=True)
        data = {'xpath': "/title/1", 'attribute': "date"}
        self.valid_form(data, pf, 3, "/title/1/@date")

    def test_value_without_attribute(self):
        pf = self.create_profile_field(attribute=True, attribute_value=True)
        data = {'xpath': "/title/1", 'attribute_value': "display"}
        self.invalid_form(data, pf, 1)

    def test_xpath_without_either(self):
        pf = self.create_profile_field(attribute=True, attribute_value=True)
        data = {'xpath': "/title/1"} 
        self.valid_form(data, pf, 3, "/title/1")

    def test_xpath_with_attribute_value_invalid(self):
        pf = self.create_profile_field(attribute=True, attribute_value=True)
        self.invalid_form({}, pf, 1)

    def test_instance(self):
        pf = self.create_profile_field(instance=True)
        data = {'xpath': '/title/0', 'instance': 2}
        self.valid_form(data, pf, 2, "/title/0", instance_number=2)

    def test_instance_all(self):
        pf = self.create_profile_field(instance=True, instance_all=True)
        data = {'xpath': '/identifier/0', 'instance': -1}
        self.valid_form(data, pf, 2, "/identifier/0", instance_number=-1)

    def test_no_delimiter(self):
        pf = self.create_profile_field(delimiter=True)
        data = {'xpath': '/subject/12', 'has_delimiter': False}
        self.valid_form(data, pf, 3, "/subject/12")

    def test_delimiter(self):
        pf = self.create_profile_field(delimiter=True)
        data = {'xpath': '/subject/12', 'has_delimiter': True, 'delimiter': ";"}
        self.valid_form(data, pf, 3, "/subject/12", delimiter=";")

    def test_delimiter_invalid(self):
        pf = self.create_profile_field(delimiter=True)
        self.invalid_form({'xpath': '/subject/12', 'has_delimiter': True}, pf, 1)

    def test_complex_xpath(self):
        pf = self.create_profile_field(complx=True)
        data = {'use_xpath': True, 'xpath': "/collection/name"}
        self.valid_form(data, pf, 3, "/collection/name")

    def test_complex_default(self):
        pf = self.create_profile_field(complx=True)
        data = {'use_xpath': False, 'default': "Archival Collection ABC"}
        self.valid_form(data, pf, 3, None, default_text="Archival Collection ABC")

    def test_complex_xpath_invalid(self):
        pf = self.create_profile_field(complx=True)
        self.invalid_form({'use_xpath': True}, pf, 1)

    def test_complex_default_invalid(self):
        pf = self.create_profile_field(complx=True)
        self.invalid_form({'use_xpath': False}, pf, 1)

    def test_filename_xpath(self):
        pf = self.create_profile_field(filename=True)
        data = {'use_xpath': True, 'xpath': "/local/identifier"}
        self.valid_form(data, pf, 2, "/local/identifier")

    def test_filename_filename(self):
        pf = self.create_profile_field(filename=True)
        data = {'use_xpath': False}
        self.valid_form(data, pf, 2, None, use_filename=True)

    def test_filename_xpath_invalid(self):
        pf = self.create_profile_field(filename=True)
        self.invalid_form({'use_xpath': True}, pf, 1)
