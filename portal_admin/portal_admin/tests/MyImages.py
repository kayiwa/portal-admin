import os, shutil, tempfile
from django.test import TestCase
from portal_admin.utils import make_thumbnail, make_main_image
import portal_admin.settings as settings
from PIL import Image

class Images(TestCase):
    '''
    I use a suite of generated images of many different sizes, and make sure that when thumbnails and main derivatives are generated, the thumbnails match exactly with what is expected.

    '''
    def setUp(self):
        self.tmpdir = tempfile.gettempdir()

        # these would be set in portal_admin.settings
        # overridden here for encapsulation
        self.THUMBNAIL_SIZE = (300, 300)
        self.IMAGE_SIZE_MAIN = (1600, 1200)

        self.test_img_dir = '{}/{}'.format(
            settings.BASE_DIR,
            'portal_admin/tests/test_data/test_images')

        # False means image sizes are not expected to change.
        self.images = [
           {'name': '100x200.jpg',   'thumb': False,     'main': False},
           {'name': '200x100.jpg',   'thumb': False,     'main': False},
           {'name': '200x200.jpg',   'thumb': False,     'main': False},
           {'name': '200x400.jpg',   'thumb': False,     'main': False},
           {'name': '200x10000.jpg', 'thumb': False,     'main': False},
           {'name': '300x300.jpg',   'thumb': False,     'main': False},
           {'name': '300x600.jpg',   'thumb': False,     'main': False},
           {'name': '400x200.jpg',   'thumb': False,     'main': False},
           {'name': '400x400.jpg',   'thumb': (300,300), 'main': False},
           {'name': '600x300.jpg',   'thumb': False,     'main': False},
           {'name': '1000x1000.jpg', 'thumb': (300,300), 'main': False},
           {'name': '1100x1600.jpg', 'thumb': (300,436), 'main': False},
           {'name': '1100x1700.jpg', 'thumb': (300,463), 'main': False},
           {'name': '1200x1200.jpg', 'thumb': (300,300), 'main': False},
           {'name': '1200x1500.jpg', 'thumb': (300,375), 'main': False},
           {'name': '1200x1600.jpg', 'thumb': (300,400), 'main': False},
           {'name': '1200x1700.jpg', 'thumb': (300,425), 'main': False},
           {'name': '1200x3600.jpg', 'thumb': (300,900), 'main': False},
           {'name': '1300x1500.jpg', 'thumb': (300,346), 'main': False},
           {'name': '1300x1600.jpg', 'thumb': (300,369), 'main': False},
           {'name': '1300x1700.jpg', 'thumb': (300,392), 'main': (1223,1600)},
           {'name': '1400x1400.jpg', 'thumb': (300,300), 'main': False},
           {'name': '1500x1100.jpg', 'thumb': (409,300), 'main': False},
           {'name': '1500x1200.jpg', 'thumb': (375,300), 'main': False},
           {'name': '1500x1300.jpg', 'thumb': (346,300), 'main': False},
           {'name': '1600x1100.jpg', 'thumb': (436,300), 'main': False},
           {'name': '1600x1200.jpg', 'thumb': (400,300), 'main': False},
           {'name': '1600x1300.jpg', 'thumb': (369,300), 'main': False},
           {'name': '1600x1600.jpg', 'thumb': (300,300), 'main': False},
           {'name': '1700x1100.jpg', 'thumb': (463,300), 'main': False},
           {'name': '1700x1200.jpg', 'thumb': (425,300), 'main': False},
           {'name': '1700x1300.jpg', 'thumb': (392,300), 'main': (1600,1223)},
           {'name': '1800x1800.jpg', 'thumb': (300,300), 'main': (1600,1600)},
           {'name': '1800x3600.jpg', 'thumb': (300,600), 'main': (1200,2400)},
           {'name': '2000x2000.jpg', 'thumb': (300,300), 'main': (1600,1600)},
           {'name': '2400x3600.jpg', 'thumb': (300,450), 'main': (1200,1800)},
           {'name': '3600x1200.jpg', 'thumb': (900,300), 'main': False},
           {'name': '3600x1800.jpg', 'thumb': (600,300), 'main': (2400,1200)},
           {'name': '3600x2400.jpg', 'thumb': (450,300), 'main': (1800,1200)},
           {'name': '10000x200.jpg', 'thumb': False,     'main': False}
        ]

        if not os.path.exists(self.tmpdir):
            os.makedirs(self.tmpdir)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)


    def test_make_thumbs(self):
        '''
        Currently the thumbs are set at 300x300px, which means that no thumbnail should have less than 300 in either dimension, unless the image already had either dimension less than 300.
        '''
        # iterate over test images and create a thumbnail
        for img in self.images:
            source = '{}/{}'.format(self.test_img_dir, img['name'])
            dest = '{}/{}'.format(self.tmpdir, img['name'])
            make_thumbnail(source, dest, self.THUMBNAIL_SIZE)

        # iterate over thumbnails and make sure they are the correct sizes
        for img in self.images:
            source = '{}/{}'.format(self.test_img_dir, img['name'])
            dest = '{}/{}'.format(self.tmpdir, img['name'])

            # make sure the thumbnails were actually created
            self.assertTrue(os.path.isfile(source))
            self.assertTrue(os.path.isfile(dest))

            i_source = Image.open(source)
            i_dest   = Image.open(dest)

            size_original = i_source.size
            size_expected = img['thumb']
            size_actual   = i_dest.size

            # if false, then no change
            if size_expected == False:
                self.assertEqual(size_actual, size_original)
            else:
                self.assertEqual(size_actual, size_expected)


    def test_make_main_derivatives(self):
        '''
        Currently the derivatives are set at 1600x1200px, which means that no thumbnail should have less than 300 in either dimension, unless the image already had either dimension less than 300.
        '''
        # iterate over test images and create a derivative
        for img in self.images:
            source = '{}/{}'.format(self.test_img_dir, img['name'])
            dest = '{}/{}'.format(self.tmpdir, img['name'])
            make_main_image(source, dest, self.IMAGE_SIZE_MAIN)

        # iterate over derivatives and make sure they are the correct sizes
        for img in self.images:
            source = '{}/{}'.format(self.test_img_dir, img['name'])
            dest = '{}/{}'.format(self.tmpdir, img['name'])

            # make sure the derivatives were actually created
            self.assertTrue(os.path.isfile(source))
            self.assertTrue(os.path.isfile(dest))

            i_source = Image.open(source)
            i_dest   = Image.open(dest)

            size_expected = img['main']

            try:
                # if false, then no change
                if size_expected == False:
                    self.assertEqual(i_dest.size, i_source.size)
                else:
                    self.assertEqual(i_dest.size, size_expected)
            except AssertionError:
                print("AssertionError: source: {}  expect: {}  dest: {}".format(i_source.size, size_expected, i_dest.size))
                raise
