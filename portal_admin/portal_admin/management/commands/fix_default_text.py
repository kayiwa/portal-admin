from django.core.management.base import BaseCommand, CommandError

from portal_admin.models import DigObject, ValueElement, ImportedValue

class Command(BaseCommand):

    def handle(self, *args, **options):
        for d in DigObject.objects.all():
            sources = d.profile.elementsource_set.exclude(default_text=None).exclude(default_text="")
            for s in sources:
                v, created = ValueElement.objects.get_or_create(element=s.default_text)
                if created:
                    print("Created value element:\n\t\"{}\"".format(v.element))
                values = d.importedvalue_set.filter(dcxml_format=s.dcxml_format, value=v)
                if values.count() == 0:
                    i = ImportedValue.objects.create(value=v,
                                                     digobject=d,
                                                     dcxml_format=s.dcxml_format)
                    print("Created imported value:\n\t{}".format(i))
                    print("Profile: {}, Insitution: {}".format(d.profile.pk, d.profile.institution.xtf_name)) 
