from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from portal_admin.models import Institution
from portal_admin import processor

class Command(BaseCommand):
    can_import_settings = True

    def handle(self, *args, **options):
        for i in Institution.objects.all():
            path = processor.generate_institution_dcxml(i.pk)
            print(path)
            
