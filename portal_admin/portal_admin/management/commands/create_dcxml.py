from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from portal_admin.models import DigObjectContext
from portal_admin import processor

class Command(BaseCommand):
    args = '<context_id>'
    can_import_settings = True

    def handle(self, *args, **options):
        context_id = args[0]
        for d in DigObjectContext.objects.filter(context=context_id):
            path = processor.generate_dc_xml(d.digobject.pk, context_id)
            print(path)
