from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from portal_admin.models import DigObjectContext
from portal_admin.utils import ensure_symlinks

class Command(BaseCommand):
    args = '<context_id>'
    can_import_settings = True

    def handle(self, *args, **options):
        context_id = args[0]

        for obj in DigObjectContext.objects.filter(digobject_status__status="Previewed", context=context_id):
            ensure_symlinks(obj.digobject.id, context_id, 'Previewed')

        for obj in DigObjectContext.objects.filter(digobject_status__status="Published", context=context_id):
            ensure_symlinks(obj.digobject.id, context_id, 'Previewed')
            ensure_symlinks(obj.digobject.id, context_id, 'Published')
