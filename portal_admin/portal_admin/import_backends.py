import shutil, os

from django.conf import settings
from portal_admin import utils

from boto.s3.connection import S3Connection
from boto.s3.key import Key

class AmazonS3Backend(object):
    def transfer(self, digobj, context_id, record, files):
        dest_path = utils.digobj_path(digobj.id, context_id)
        utils.ensure_dirs(dest_path)
        src_data_filename = record.path + record.filename
        dest_data_filename = dest_path.format("data") + "{}.xml".format(digobj.ark)
        dest_file_path = dest_path.format(settings.FILE_DEST[digobj.digobject_form._type])

        shutil.copyfile(src_data_filename, dest_data_filename)
        if not digobj.profile.is_remote:
            for f in files:
                if f[1] in settings.ALLOWED_EXTS['images']:
                    # setup
                    img_src = record.path + f[2]
                    dest_path = "{}/{}/{}".format(digobj.digobject_form._type,
                                                  digobj.profile.institution.xtf_name,
                                                  digobj.profile.id)
                    img_dest = "{}/{}.jpg".format(dest_path, digobj.ark)

                    # thumbs
                    src_thumbs = record.path + "thumbs/"
                    os.makedirs(src_thumbs, exist_ok=True)
                    thumb_src = "{}{}.jpg".format(src_thumbs, digobj.ark)
                    utils.make_thumbnail(img_src, thumb_src)
                    thumb_dest = "{}/thumbs/{}.jpg".format(dest_path, digobj.ark)

                    # derivs, in place (fyi: destructive)
                    utils.make_main_image(img_src, img_src)

                    # send to amazon
                    self.send_to_amazon(img_src, img_dest)
                    self.send_to_amazon(thumb_src, thumb_dest)

                    # clean up
                    os.remove(img_src) # if we don't remove it now it'll think that we didn't find metadata for it later
                else:
                    file_src = record.path + f[2]
                    shutil.move(file_src, dest_file_path + digobj.ark + '.' + f[1])

    def get_bucket(self):
        c = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                         settings.AWS_SECRET_ACCESS_KEY)
        return c.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)

    def send_to_amazon(self, src, dest):
        b = self.get_bucket()
        k = Key(b)
        k.key = settings.MEDIAFILES_LOCATION + "/" + dest
        k.set_contents_from_filename(src)

    def delete_files(self, digobj, statuses):
        path = settings.FILE_ROOT + '{}' + \
               "/data/{}/{}/{}/".format(digobj.digobject_form._type,
                                        digobj.profile.institution.xtf_name,
                                        digobj.profile.id)
        data_paths = {}
        for s in statuses:
            d = path.format(s.status)
            if os.path.isdir(d):
                for f in os.listdir(d):
                    if f[:f.rfind(".")] == digobj.ark:
                        os.remove(d + f)
                        if not d in data_paths:
                            data_paths.update({d: s.status})

        b = self.get_bucket()
        dest_path = "{}/{}/{}/{}/".format(settings.MEDIAFILES_LOCATION,
                                          digobj.digobject_form._type,
                                          digobj.profile.institution.xtf_name,
                                          digobj.profile.id)
        for f in digobj.file_set.all():
            self.delete_key(b, (dest_path + f.filename_new))
            self.delete_key(b, (dest_path + "thumbs/" + f.filename_new))
        return data_paths

    def delete_key(self, bucket, key):
        k = bucket.get_key(key)
        if k:
            k.delete()

class LocalFileBackend(object):
    def transfer(self, digobj, context_id, record, files):
        dest_path = utils.digobj_path(digobj.id, context_id)
        utils.ensure_dirs(dest_path)

        shutil.copyfile((record.path + record.filename), (dest_path.format("data") + digobj.ark + '.xml'))
        if not digobj.profile.is_remote:
            file_path = dest_path.format(settings.FILE_DEST[digobj.digobject_form._type])
            for f in files:
                file_src = record.path + f[2]
                if f[1] in settings.ALLOWED_EXTS['images']:
                    file_dest = file_path + digobj.ark + '.jpg'
                    shutil.move(file_src, file_dest)
                    # thumbs
                    utils.make_thumbnail(file_dest, "{}thumbs/{}.jpg".format(file_path, digobj.ark))

                    # derivs, in place (fyi: destructive)
                    utils.make_main_image(file_dest, file_dest)
                else:
                    file_dest = file_path + digobj.ark + "." + f[1]
                    shutil.move(file_src, file_dest)

    def delete_files(self, digobj, statuses):
        path = settings.FILE_ROOT + '{}/{}' + "/{}/{}/{}/".format(digobj.digobject_form._type,
                                                                  digobj.profile.institution.xtf_name,
                                                                  digobj.profile.id)
        data_paths = {}
        for s in statuses:
            dirs = [path.format(s.status, "data"),
                    path.format(s.status, "media"),
                    path.format(s.status, "media") + "thumbs/"]
            d = path.format(s.status, "data")
            if os.path.isdir(d):
                for f in os.listdir(d):
                    if f[:f.rfind(".")] == digobj.ark:
                        os.remove(d + f)
                        if not d in data_paths:
                            data_paths.update({d: s.status})
            d = path.format(s.status, "media")
            if os.path.isdir(d):
                for f in os.listdir(d):
                    if f[:f.rfind(".")] == digobj.ark:
                        os.remove(d + f)
                        os.remove(d + "thumbs/" + f)
        return data_paths

