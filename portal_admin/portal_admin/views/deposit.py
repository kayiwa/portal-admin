import os, shutil, tempfile, json

from django.conf import settings

from django.shortcuts import redirect
from django.http import HttpResponse

from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.files.storage import FileSystemStorage
from django.views.generic import DetailView, TemplateView

from django.views.decorators.http import require_POST
from jfu.http import upload_receive, UploadResponse, JFUResponse

from portal_admin.models import (DigObjectForm,
                                 Institution,
                                 Location,
                                 Profile,
                                 ProfileField,)

from portal_admin.forms.deposit import (BlankForm,
                                        ImageMetadataForm,
                                        ImageSubmissionForm,
                                        LocationForm,
                                        ProfileFieldFormSet,
                                        ProfileForm,
                                        SampleFileForm,
                                        UploadForm,)

from portal_admin import validation
from portal_admin.tasks import process_input

#
# Sample file validation API
#

def validate_xpath(request):
    if request.is_ajax():
        if request.method == 'POST':
            data = json.loads(request.body.decode('utf-8'))
            filename = os.path.join(settings.SAMPLE_FILE_ROOT, data['filename'])
            try:
                value = validation.validate_with_sample_file(filename, data)
                result = {'result': 'success', 'value': value}
            except validation.ValidationError as e:
                result = {'result': 'danger', 'value': str(e)}
            return HttpResponse(json.dumps(result), content_type='application/json')
    return HttpResponse(json.dumps({'result': 'danger', 'value': 'Invalid request'}), 
                        content_type='application/json')

def validate_separator(request):
    if request.is_ajax():
        if request.method == 'POST':
            data = json.loads(request.body.decode('utf-8'))
            filename = os.path.join(settings.SAMPLE_FILE_ROOT, data['filename'])
            try:
                value = validation.validate_separator(filename, data['split_node'])
                result = {'result': 'success', 'value': value}
            except validation.ValidationError as e:
                result = {'result': 'danger', 'value': str(e)}
            return HttpResponse(json.dumps(result), content_type='application/json')
    return HttpResponse(json.dumps({'result': 'danger', 'value': 'Invalid request'}), 
                        content_type='application/json')

def validate_filesource(request):
    if request.is_ajax():
        if request.method == 'POST':
            data = json.loads(request.body.decode('utf-8'))
            filename = os.path.join(settings.SAMPLE_FILE_ROOT, data['filename'])
            try:
                if "split_node" in data:
                    value = validation.validate_separator(filename, data['split_node'])
                value = validation.validate_filesource(filename, data)
                result = {'result': 'success', 'value': value}
            except validation.ValidationError as e:
                result = {'result': 'danger', 'value': str(e)}
            return HttpResponse(json.dumps(result), content_type='application/json')
    return HttpResponse(json.dumps({'result': 'danger', 'value': 'Invalid request'}), 
                        content_type='application/json')

#
# Upload Wizard
#

@require_POST
def upload(request):
    upload = upload_receive(request)
    upload_dir = request.POST['upload_dir']
    with open(os.path.join(upload_dir, upload.name), 'wb+') as dest:
        for c in upload.chunks():
            dest.write(c)
    file_dict = {'name': upload.name,
                 'size': upload.size}
    return UploadResponse(request, file_dict)

@require_POST
def upload_delete(request):
    success = False
    if 'upload_dir' in request.POST:
        upload_dir = request.POST['upload_dir']
        if os.path.isdir(upload_dir):
            shutil.rmtree(upload_dir)
            success = True
    return JFUResponse(request, success)

def has_multiple_locations(wizard):
    profile = Profile.objects.get(pk=wizard.kwargs['pk'])
    return profile.institution.location_set.count() > 1

class UploadRulesView(TemplateView):
    template_name = "deposit/upload_rules.html"

    def get_context_data(self, **kwargs):
        context = super(UploadRulesView, self).get_context_data(**kwargs)
        profile = Profile.objects.get(pk=self.kwargs.get('pk'))
        rules = []
        for f in profile.digobject_form.profilefield_set.all().order_by('field_order'):
            rules.append({'field': f,
                          'elements': f.dcxml_format.elementsource_set.filter(profile=profile)})
        context.update({'profile': profile,
                        'rules': rules})
        return context

class UploadWizard(SessionWizardView):
    form_list = [LocationForm, UploadForm]
    templates = ["deposit/location.html", "deposit/upload.html"]
    condition_dict = {'0': has_multiple_locations}
    file_storage = FileSystemStorage(location=settings.SAMPLE_FILE_ROOT)

    def get_template_names(self):
        return self.templates[int(self.steps.current)]

    def get_context_data(self, form, **kwargs):
        context = super(UploadWizard, self).get_context_data(form=form, **kwargs)
        profile = Profile.objects.get(pk=self.kwargs.get('pk'))
        context.update({'profile': profile})
        if self.steps.current == '1':
            context.update({'accepted_mime_types': settings.ALLOWED_EXTS[profile.digobject_form._type] + ["xml"],
                            'upload_dir': form.initial['upload_dir']})
        return context

    def get_form_kwargs(self, step):
        if step == '0':
            return {'institution': Institution.objects.get(pk=self.request.session['institution'])}
        else:
            return {}

    def get_form_initial(self, step):
        if step == '1':
            return {'upload_dir': tempfile.mkdtemp()}
        else:
            return {}

    def done(self, form_list, **kwargs):
        context_id = self.request.session['context']
        profile_id = kwargs.get('pk')

        if has_multiple_locations(self):
            location = form_list[0].cleaned_data['location']
            upload_dir = form_list[1].cleaned_data['upload_dir']
        else:
            profile = Profile.objects.get(pk=profile_id)
            location = Location.objects.get(institution=profile.institution)
            upload_dir = form_list[0].cleaned_data['upload_dir']

        # Turn off for local use, without Celery or Redis
        process_input.delay(self.request.user.id, profile_id, context_id, upload_dir,
                            location.id, self.request.get_host())
        return redirect('uploaded', pk=profile_id)

class UploadedView(DetailView):
    template_name = "deposit/uploaded.html"
    context_object_name = "profile"
    model = Profile

    def get_context_data(self, **kwargs):
        context = super(UploadedView, self).get_context_data(**kwargs)
        profile = kwargs.get('object')
        context['profiles'] = profile.digobject_form.profile_set\
                                                    .filter(institution=profile.institution)\
                                                    .exclude(pk=profile.pk)
        return context

#
# Rules creation wizard
#

def show_imagemetadata_condition(wizard):
    digobject_form = DigObjectForm.objects.get(pk=wizard.kwargs.get("pk"))
    return (digobject_form._type == "images")

def has_profile_fields(wizard):
    digobjectform_id = wizard.kwargs.get("pk")
    return ProfileField.objects.filter(digobject_form__id=digobjectform_id).count() > 0

class CreateProfileWizard(SessionWizardView):
    form_list = [BlankForm, ProfileForm, SampleFileForm, ImageSubmissionForm, ImageMetadataForm, ProfileFieldFormSet]
    templates = ["deposit/instructions.html",
                 "deposit/profile.html",
                 "deposit/sample_file.html",
                 "deposit/image_submission.html",
                 "deposit/image_metadata.html",
                 "deposit/profile_fields.html"]
    step_names = ["Prepare",
                  "Name & Describe Rule Set",
                  "Sample File Submission",
                  "Image Submission",
                  "Metadata Information",
                  "Define Import Fields"]

    condition_dict = {'3': show_imagemetadata_condition,
                      '4': show_imagemetadata_condition,
                      '5': has_profile_fields}
    file_storage = FileSystemStorage(location=settings.SAMPLE_FILE_ROOT)

    def get_form_instance(self, step):
        if step == '1':
            return Profile()
        else:
            return self.instance_dict.get(step, None)

    def get_form_initial(self, step):
        if step == '1':
            return {'institution': Institution.objects.get(pk=self.request.session['institution'])}
        else:
            return self.initial_dict.get(step, {})

    def get_context_data(self, form, **kwargs):
        context = super(CreateProfileWizard, self).get_context_data(form=form, **kwargs)
        digobject_form = DigObjectForm.objects.get(pk=self.kwargs.get("pk"))
        context.update({'step_name': self.step_names[int(self.steps.current)],
                        'digobject_type': digobject_form.dcxml_value})

        if self.steps.current == '4':
            fdata = self.storage.get_step_files('2')
            context.update({'file': fdata.get('2-sample_file', False)})
        if self.steps.current == '5':
            fdata = self.storage.get_step_files('2')
            f = fdata.get('2-sample_file', False)
            idata = self.storage.get_step_data('4')
            sn = idata.get('4-record_separator', False) if idata else False
            context.update({'file': f, 'split_node': sn})
        return context

    def get_template_names(self):
        return self.templates[int(self.steps.current)]

    def get_form_kwargs(self, step):
        if step == '5':
            digobjectform_id = self.kwargs.get("pk")
            return {'profile_fields': ProfileField.objects.filter(digobject_form__id=digobjectform_id).order_by('field_order')}
        elif step == '4':
            digobject_form = DigObjectForm.objects.get(pk=self.kwargs.get("pk"))
            idata = self.storage.get_step_data('3')
            is_remote = idata.get('3-image_submission', False) == 'True' if idata else False
            return {'use_splitnode': digobject_form.manifest_format in settings.SPLITNODE_FORMATS,
                    'is_remote': is_remote}
        else:
            return {}

    def done(self, form_list, **kwargs):
        for f in form_list:
            if isinstance(f, ProfileForm):
                f.instance.digobject_form = DigObjectForm.objects.get(pk=kwargs.get("pk"))
                profile = f.save()
            elif isinstance(f, ImageMetadataForm):
                file_source, profile = f.save(profile)
            elif isinstance(f, ProfileFieldFormSet):
                profile_fields = f.save(profile)
        self.file_storage.delete(self.storage.get_step_files('2').get('2-sample_file').name)
        return redirect("profile_saved", pk=profile.id)

class ProfileSavedView(DetailView):
    template_name = "deposit/profile_saved.html"
    context_object_name = "profile"
    model = Profile

    def get_context_data(self, **kwargs):
        context = super(ProfileSavedView, self).get_context_data(**kwargs)
        profile = kwargs.get('object')
        fields = []
        for f in profile.digobject_form.profilefield_set.all().order_by('field_order'):
            fields.append({'field': f, 'elements': f.dcxml_format.elementsource_set.filter(profile=profile)})
        context['fields'] = fields
        context['digobject_type'] = profile.digobject_form.dcxml_value
        context['profiles'] = profile.digobject_form.profile_set.filter(institution=self.request.session['institution'])
        return context

#
# Landing page
#

class DepositView(TemplateView):
    template_name = "deposit/deposit.html"

    def get_context_data(self, **kwargs):
        context = super(DepositView, self).get_context_data(**kwargs)
        types = []
        institution = Institution.objects.get(pk=self.request.session['institution'])
        manager_roles = self.request.user.userrole_set.filter(context=self.request.session['context'],
                                                              role__rolename="manager")
        if manager_roles.count() > 0:
            userrole = manager_roles[0]
        else:
            userroles = self.request.user.userrole_set.filter(
                context=self.request.session['context'],
                institution=self.request.session['institution'])
            userrole = userroles[0]
        allowed = userrole.role.digobjectforms.all()
        for dot in DigObjectForm.objects.values_list('dcxml_value', '_type').distinct():
            types.append({'type': dot[1],
                          'value': dot[0],
                          'profiles': Profile.objects.filter(digobject_form__dcxml_value=dot[0],
                                                             institution=institution),
                          'forms': DigObjectForm.objects.filter(dcxml_value=dot[0], pk__in=allowed)})
        context['types'] = types
        return context
