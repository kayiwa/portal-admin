from django.views.generic import TemplateView, DeleteView
from django.shortcuts import redirect

from portal_admin.views.browse import BrowseRecordsView
from portal_admin.models import Connection, DigObjectContext, DigObjectForm

class ConnectView(BrowseRecordsView):
    template_name = "connect/connect.html"

    def post(self, request, *args, **kwargs):
        if "connect_cancel" in self.request.POST.values():
            self.request.session.pop('objs_active', None)
            self.request.session.pop('parent_objs', None)
            self.request.session.pop('child_objs', None)
            return redirect('connect')
        else:
            return super(ConnectView, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(ConnectView, self).get_form_kwargs()

        objs = self.filter_digobjects(self.request)

        # if nothing in the session, then we are choosing parents
        # so limit objects to those which can be parents
        if 'objs_active' not in self.request.session:
            formats = DigObjectForm.objects.filter(can_be_parent=True)
            objs = objs.filter(digobject_form__in=formats)

        kwargs.update({'pages': self.get_pages(objs),
                       'page': self.request.GET.get('page', 1)})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ConnectView, self).get_context_data(**kwargs)
        objs = self.filter_digobjects(self.request)

        if 'objs_active' in self.request.session:
            # if selected records exist, they're parents
            objs_active = objs.filter(pk__in=self.request.session['objs_active'])

            context.update({'title': 'Connect: Select Children',
                            'subtitle': 'Select Child Records from the List Below',
                            'action_buttons': [['select_children',
                                                'Select Child Records',
                                                'primary'],
                                               ['connect_cancel', 'Cancel'],],
                            'objs_flat': objs_active})
        else:
            # No records selected, so let's select parents
            context.update({'title': 'Connect: Select Parent',
                            'subtitle': 'Select a Parent Record from the List Below',
                            'action_buttons': [['select_parents',
                                                'Select a Parent Record',
                                                'primary']],
                            'single_select': True})
        return context

    def form_valid(self, form):
        try:
            # FIXME: Don't ever catch a key error. Test for the expected conditions
            pager_selections = self.request.POST.get('pager_selections', None).split(',')

            self.request.session['parent_objs'] = self.request.session.pop('objs_active')
            self.request.session['child_objs'] = pager_selections

            return redirect('connect_confirm')
        except KeyError:
            self.request.session['objs_active'] = form.cleaned_data['items']
            return redirect('connect')


class ConnectConfirmView(TemplateView):
    template_name = "connect/connect_confirm.html"

    def get_context_data(self, **kwargs):
        context = super(ConnectConfirmView, self).get_context_data(**kwargs)

        cid = self.request.session['context']

        # something hasn't been selected so go back to connect
        if not 'parent_objs' in self.request.session or not 'child_objs' in self.request.session:
            return redirect('connect')

        # We need to get parent and children records from the form
        # (actually, the session, since the Form only returns what's on the
        # web page; we often need more than just one page of records.)
        # Parents is/are easy: just get from the session.
        parent_objs = DigObjectContext.objects.filter(pk__in=self.request.session['parent_objs'])
        child_objs = DigObjectContext.objects.filter(pk__in=self.request.session['child_objs'])

        # These lists sort out the results for human consumption
        prohibited_connections = []
        existing_connections = []
        new_connections = []

        for parent in parent_objs:
            pobj = parent.digobject
            for child in child_objs:
                cobj = child.digobject
                if pobj == cobj:
                    prohibited_connections.append({'parent': pobj,
                                                   'child': cobj,
                                                   'reason': "a record cannot be connected to itself"})
                elif Connection.objects.filter(parent=cobj, child=pobj).exists():
                    prohibited_connections.append({'parent': pobj,
                                                   'child': cobj,
                                                   'reason': "these records are already connected in reverse"})
                elif Connection.objects.filter(parent=pobj, child=cobj).exists():
                    existing_connections.append({'parent': pobj,
                                                 'child': cobj})
                else:
                    new_connections.append(pobj.add_child(cobj))
                    parent.set_dirty(True)
                    child.set_dirty(True)

        context.update({'title': 'Confirm Connections',
                        'prohibited_connections': prohibited_connections,
                        'existing_connections': existing_connections,
                        'new_connections': new_connections})
        return context

class ConnectDeleteView(DeleteView):
    model = Connection

    def get_success_url(self):
        return self.request.META['HTTP_REFERER']

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        cid = self.request.session['context']
        DigObjectContext.objects.get(digobject=obj.parent, context__pk=cid).set_dirty(True)
        DigObjectContext.objects.get(digobject=obj.child, context__pk=cid).set_dirty(True)
        return super(ConnectDeleteView, self).delete(request, *args, **kwargs)

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(ConnectDeleteView, self).get_object()
        cid = self.request.session['context']

        return obj
