from django.views.generic import FormView, TemplateView, View

import os, datetime
from django.conf import settings

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.template import RequestContext

from portal_admin.forms.admin import DateForm, ContactForm

from portal_admin.models import (Context,
                                 DigObjectContext,
                                 DigObjectStatus,
                                 Institution,
                                 MetaEvent,
                                 Profile,
                                 UserRole,)

from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy
from django.template.loader import render_to_string

from django.db.models import Min, Count, Q

#
# Landing page
#

class OverviewView(TemplateView):
    template_name = "admin/overview.html"

    def get_context_data(self, **kwargs):
        institution = Institution.objects.get(pk=self.request.session['institution'])
        context = Context.objects.get(pk=self.request.session['context'])
        admins = UserRole.objects.filter(institution=institution,
                                         context=context,
                                         role__rolename="localadmin")
        num_archivists = UserRole.objects.filter(institution=institution,
                                                 context=context,
                                                 role__rolename="archivist").count()

        status_names = ['Deposited', 'Previewed', 'Published']
        statuses = {'Other': {'count': 0},
                    'Total': {'count': 0}}

        for s in DigObjectStatus.objects.all():
            if s.status in status_names:
                statuses.update({s.status: {'count': 0, 'pk': s.pk}})

        # active statuses
        counts = DigObjectContext.objects.filter(context=context,
                                                 digobject__profile__institution=institution)\
                                         .values('digobject__profile',
                                                 'digobject_status__status')\
                                         .annotate(status_count=Count('pk'))

        count_dict = {}
        for c in counts:
            profile_id = c['digobject__profile']
            status = c['digobject_status__status']
            count = c['status_count']
            if not profile_id in count_dict:
                count_dict.update({profile_id: {'Deposited': 0,
                                                'Previewed': 0,
                                                'Published': 0,
                                                'Other': 0,
                                                'Total': 0}})
            if status in count_dict[profile_id]:
                count_dict[profile_id][status] += count
                statuses[status]['count'] += count
            else:
                count_dict[profile_id]['Other'] += count
                statuses['Other']['count'] += count
            count_dict[profile_id]['Total'] += count
            statuses['Total']['count'] += count

        profile_list = []
        for p in Profile.objects.filter(institution=institution).order_by('name'):
            profile = {'name': p.name,
                       'pk': p.pk}
            if p.pk in count_dict:
                profile.update(count_dict[p.pk])
            profile_list.append(profile)

        modified_records = DigObjectContext.objects\
                                           .filter(context=context,
                                                   digobject__profile__institution=institution,
                                                   dirty=True)\
                                           .filter(Q(digobject_status__status="Previewed") |
                                                   Q(digobject_status__status="Published")).count()
        return {'institution': institution,
                'context': context,
                'num_archivists': num_archivists,
                'admins': admins,
                'profile_list': profile_list,
                'statuses': statuses,
                'modified_records': modified_records}

#
# Error Logs
#

class ErrorLogsView(View):
    template_name = "admin/error_logs.html"

    def get(self, request, *args, **kwargs):
        data = self.get_context_data(**kwargs)
        return render_to_response(self.template_name, data, RequestContext(request))

    def post(self, request, *args, **kwargs):
        data = self.get_context_data(**kwargs)
        return render_to_response(self.template_name, data, RequestContext(request))

    def get_context_data(self, **kwargs):
        c = self.request.session['context']
        i = self.request.session['institution']
        events = MetaEvent.objects.filter(context=c, profile__institution=i, error_log_url__isnull=False)
        form = DateForm(self.request.POST)
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
        else:
            start_date = events.aggregate(min_date=Min('datestamp'))['min_date'] or datetime.date.today()
            end_date = datetime.date.today()
        return {'institution': Institution.objects.get(pk=i),
                'context': Context.objects.get(pk=c),
                'events': events.filter(datestamp__gte=start_date,
                                        # we add a day to end date because datestamp is datetime and straight dates
                                        # are interpreted as midnight on the given day. ev 2014-8-20
                                        datestamp__lte=(end_date + datetime.timedelta(1))).order_by('-datestamp'),
                'date_form': DateForm(initial={'start_date': start_date, 'end_date': end_date})}

class ErrorLogDownloadView(View):
    def get_mimetype(self, extension):
        if extension == "zip":
            return 'application/octet-stream'
        elif extension == "xml":
            return 'application/xml'
        elif extension == "txt":
            return 'text/plain'
        else:
            raise Http404

    def get(self, request, pk, *args, **kwargs):
        meta_event = get_object_or_404(MetaEvent, pk=pk)
        filename = meta_event.error_log_url
        path = os.path.join(settings.FILE_ROOT, meta_event.error_log_url)
        mimetype = self.get_mimetype(filename.split(".")[-1])
        if os.path.isfile(path):
            f = open(path, 'rb')
            response = HttpResponse(f.read(), mimetype=mimetype)
            f.close()
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
            response['Content-Length'] = os.path.getsize(path)
            return response
        else:
            raise Http404

class DeleteErrorLogView(View):
    def get(self, request, pk, *args, **kwargs):
        meta_event = get_object_or_404(MetaEvent, pk=pk)
        os.remove(os.path.join(settings.FILE_ROOT, meta_event.error_log_url))
        meta_event.deposit_error_events().delete()
        meta_event.error_log_url = None
        meta_event.save()
        if meta_event.event_set.count() == 0:
            meta_event.delete()
        return redirect('error_logs')

#
# Help
#

class HelpView(TemplateView):
    template_name = "admin/help.html"


class HelpFormView(FormView):
    form_class = ContactForm
    template_name = 'admin/help_form.html'
    success_url = reverse_lazy('help_sent')

    def form_valid(self, form):
        context = Context.objects.get(pk=self.request.session['context'])
        data = {'user': self.request.user,
                'message': form.cleaned_data['message']}
        send_mail(subject="{} help request from {}".format(context.name, self.request.user.email),
                  message=render_to_string('help/help_email.txt', data),
                  from_email=self.request.user.email,
                  recipient_list=[context.help_email])
        return super(HelpFormView, self).form_valid(form)

class HelpSentView(TemplateView):
    template_name = "admin/help_sent.html"
