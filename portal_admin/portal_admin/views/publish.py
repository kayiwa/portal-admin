from django.views.generic import TemplateView, UpdateView

from portal_admin.views.browse import FilterDigObjectsMixin, BrowseRecordsView
from portal_admin.models import Context, DigObject, DigObjectContext

from portal_admin.tasks import start_indexer
from django.db.models import Q

class PublishView(BrowseRecordsView):
    template_name = "preview/preview.html"

    def get_context_data(self, **kwargs):
        context = super(PublishView, self).get_context_data(**kwargs)

        action_buttons = [['publish', 'Publish'],]

        context.update({'title': 'Publish',
                        'action_buttons': action_buttons})
        return context

class PublishRecordView(UpdateView):
    template_name = "publish/publish_confirm.html"
    model = DigObject

    def get_context_data(self, **kwargs):
        context = super(PublishRecordView, self).get_context_data(**kwargs)
        obj = context['object']
        hcid = self.request.session['context']
        doc = obj.digobjectcontext_set.get(context__pk=hcid)

        publish_objs, needs_preview, reindex_objs, neither_objs, index_id = [], [], [], [], []

        if doc.digobject_status.status == "Deposited":
            needs_preview = [obj]
        elif doc.digobject_status.status == 'Previewed':
            publish_objs = [obj]
            index_id = [obj.pk]
        elif doc.digobject_status.status == 'Published':
            reindex_objs = [obj]
            index_id = [obj.pk]
        elif doc.digobject_status.status == 'Retracted' or doc.digobject_status.status == 'Deleted':
            neither_objs = [obj]

        if len(index_id) > 0:
            start_indexer.delay(self.request.user.id,
                                hcid,
                                index_id,
                                self.request.get_host(),
                                'Published')

        context.update({'title': 'Records Published',
                        'publish_objs': publish_objs,
                        'needs_preview': needs_preview,
                        'reindex_objs': reindex_objs,
                        'neither_objs': neither_objs})
        return context

class PublishConfirmView(FilterDigObjectsMixin, TemplateView):
    template_name = "publish/publish_confirm.html"

    def get_context_data(self, **kwargs):
        context = super(PublishConfirmView, self).get_context_data(**kwargs)

        hopper_context_id = self.request.session['context']

        contexts = DigObjectContext.objects\
                                   .filter(digobject__in=self.request.session['objs'],
                                           context__pk=hopper_context_id)

        publish_objs = contexts.filter(Q(digobject_status__status="Previewed"))\
                               .values_list('digobject', flat=True)

        needs_preview = contexts.filter(Q(digobject_status__status="Deposited"))\
                                .values_list('digobject', flat=True)


        reindex_objs = contexts.filter(digobject_status__status="Published")\
                               .values_list('digobject', flat=True)

        neither_objs = contexts.filter(Q(digobject_status__status="Retracted") |
                                       Q(digobject_status__status="Deleted"))\
                               .values_list('digobject', flat=True)

        objs = list(publish_objs) + list(reindex_objs)

        if len(objs) > 0:
            start_indexer.delay(self.request.user.id,
                                hopper_context_id,
                                objs,
                                self.request.get_host(),
                                'Published')

        context.update({'title': 'Records Published',
                        'context': Context.objects.get(pk=hopper_context_id),
                        'publish_objs': DigObject.objects.filter(pk__in=publish_objs),
                        'needs_preview': DigObject.objects.filter(pk__in=needs_preview),
                        'reindex_objs': DigObject.objects.filter(pk__in=reindex_objs),
                        'neither_objs': DigObject.objects.filter(pk__in=neither_objs)})
        return context
