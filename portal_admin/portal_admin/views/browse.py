import os

from urllib.parse import urlencode, urlparse

from django.conf import settings
from django.shortcuts import redirect
from django.http import HttpResponse, Http404

from django.core.urlresolvers import reverse_lazy
from django.db.models import Count
from django.core.paginator import Paginator

from django.views.generic import (DeleteView,
                                  DetailView,
                                  FormView,
                                  TemplateView,
                                  UpdateView,)

from portal_admin import processor
from portal_admin import utils

from portal_admin.models import (Connection,
                                 Context,
                                 DCXMLFormat,
                                 DigObject,
                                 DigObjectContext,
                                 DigObjectFacetValue,
                                 FacetGroup,
                                 FacetValue,
                                 ImportedValue,
                                 ValueElement,)

from portal_admin.forms.record import (DigObjectFilterForm,
                                       FileFormSet,
                                       HiddenDigObjectsForm,
                                       ImportedValueFilterForm,
                                       RecordForm,
                                       SelectImportedValuesForm,)

from portal_admin.forms.tag import TagFilterForm

from portal_admin.tasks import delete_objects

class FilterDigObjectsMixin(object):

    def get_params(self, request):
        status = request.GET.get('status', False)
        t = request.GET.get('type', False)
        profile = request.GET.get('profile', False)
        dcxml_format = request.GET.get('field', False)
        q = request.GET.get('q', False)

        return {'status': status,
                't': t,
                'profile': profile,
                'dcxml_format': dcxml_format,
                'q': q}

    def filter_digobjects(self, request):
        objs = DigObject.objects.filter(profile__institution=request.session['institution'],
                                        digobjectcontext__context=request.session['context'])
        p = self.get_params(request)
        if p['status']:
            # filter: status based on selection from user
            objs = objs.filter(digobjectcontext__digobject_status=p['status'])
        if p['t']:
            # filter: images/archival based on selection from user
            objs = objs.filter(digobject_form___type=p['t'])
        if p['profile']:
            # filter: to given profile if the user has chosen one
            objs = objs.filter(profile=p['profile'])
        if p['dcxml_format']:
            # filter: to given field if the user has chosen one
            objs = objs.filter(importedvalue__dcxml_format=p['dcxml_format'])
        if p['q']:
            # filter: if query
            predicate = self.request.GET.get('predicate', "element__icontains")
            elements = ValueElement.objects.filter(**{predicate: p['q'].strip()})
            values = ImportedValue.objects.filter(value__in=elements)
            objs = objs.filter(pk__in=values.values_list("digobject", flat=True))
        return objs.distinct().order_by('title')


    def filter_values(self, request):
        p = self.get_params(request)

        # filter: since most parameter values filter through objs,
        # base most value filters on objs.
        objs = self.filter_digobjects(request)
        values = ImportedValue.objects.filter(digobject__in=objs)

        if p['status']:
            # filter: status based on selection from user
            values = values.filter() # stub
        if p['t']:
            # filter: images/archival based on selection from user
            values = values.filter() # stub
        if p['profile']:
            # filter: to given profile if the user has chosen one
            values = values.filter() # stub
        if p['dcxml_format']:
            # filter: to given field if the user has chosen one
            values = values.filter(dcxml_format=p['dcxml_format'])
        if p['q']:
            # filter: if query.  A little more explicit.
            predicate = self.request.GET.get('predicate', "element__icontains")
            elements = ValueElement.objects.filter(**{predicate: p['q']})
            values = values.filter(value__in=elements)

        # group by: value
        # aggregate: imported values per value
        values = values.values('value')\
                       .annotate(count=Count('value'))\
                       .order_by(self.request.GET.get('sort', 'value__element'))

        return values.distinct()

    # stub
    def filter_tags(self, request):
        tags = FacetValue.objects.all()
        p = self.get_params(request)
        return tags.distinct()


class BrowseView(FilterDigObjectsMixin, FormView):
    def get_pages(self, items):
        return Paginator(items, self.request.GET.get('items_per_page', 20))

    def get(self, request, *args, **kwargs):
        response = super(BrowseView, self).get(request, *args, **kwargs)
        # we only want to keep selected items when using POST
        request.session['pager_selections'] = []
        return response

    def post(self, request, *args, **kwargs):
        page_change = request.POST.get('page_change', False)
        items_per_page = request.POST.get('items_per_page', False)
        current_url = request.get_full_path()
        current_query_string = request.GET.copy()

        # for switching pages: select items, pass to session
        if request.POST.get('pager_selections'):
            # convert string to list; map strings to integers.
            pager_selections = list(map(int, 
                                        request.POST.get('pager_selections', 
                                                         False).split(',')))
            if page_change or items_per_page:
                request.session['pager_selections'] = pager_selections
                request.session['items_submitted'] = []
            else: # must be submission
                request.session['pager_selections'] = []
                request.session['items_submitted'] = pager_selections
        else:
            # don't keep anything selected if it's not directly from POST.
            request.session['pager_selections'] = []
            request.session['items_submitted'] = []

        # pager intercept redirect
        if page_change:
            current_query_string['page'] = page_change
            new_query_string = current_query_string.urlencode()
            new_url = '{}?{}'.format( request.path, new_query_string)
            return redirect(new_url)
        if items_per_page:
            current_query_string['items_per_page'] = items_per_page
            current_query_string['page'] = 1
            new_query_string = current_query_string.urlencode()
            new_url = '{}?{}'.format( request.path, new_query_string)
            return redirect(new_url)
        else:
            response = super(BrowseView, self).post(request, *args, **kwargs)
            return response

    def get_context_data(self, form, **kwargs):
        context = super(BrowseView, self).get_context_data(**kwargs)

        pager_selections = self.request.session.get('pager_selections', None)

        action_buttons = [['review',  'Review Records'],
                          ['add',     'Add Tags'],
                          ['preview', 'Preview'],
                          ['publish', 'Publish'],
                          ['delete',  'Delete'],]

        context.update({'title': 'Browse',
                        'list_unit': 'record',
                        'action_buttons': action_buttons,
                        'filter_form': DigObjectFilterForm(self.request.GET),
                        'importedvalue_form': ImportedValueFilterForm(self.request.GET),
                        'search_form': form,
                        'pager_selections': pager_selections,})
        return context


class BrowseOverView(BrowseView):
    template_name = "browse/browse_overview.html"
    form_class = ImportedValueFilterForm

    def get_context_data(self, **kwargs):
        context = super(BrowseOverView, self).get_context_data(**kwargs)
        objs = self.filter_digobjects(self.request)
        tab = self.request.GET.get('tab', 'search')

        # filter: DCXMLFormats from imported values associated with digobjects in this context/institution
        fields = DCXMLFormat.objects.filter(importedvalue__digobject__in=objs)\
                                    .exclude(field_label=None).distinct()

        # filter: facet groups associated with facet values associated with digobjects in this context/institution
        # aggregates: number of digobjectfacetvalues in this institution/context
        # sort: Alphabetical by facet_group.label
        facet_count = Count('facetvalue__digobjectfacetvalue', distinct=True)
        tf = TagFilterForm(self.request.GET)
        if tf.is_valid():
            groups = FacetGroup.objects.filter(context=self.request.session['context'],
                                               facetvalue__digobjectfacetvalue__digobject__in=objs,
                                               facetvalue__digobjectfacetvalue__is_autoassigned=\
                                               (tf.cleaned_data['assigned']=='True'))\
                                       .distinct().order_by('label')
        else:
            groups = FacetGroup.objects.filter(context=self.request.session['context'],
                                               facetvalue__digobjectfacetvalue__digobject__in=objs)\
                                       .distinct().order_by('label')

        context.update({'tab': tab,
                        'fields': fields,
                        'groups': groups})
        return context


class BrowseRecordsView(BrowseView):
    '''
    By default, without filtering, this view provides a list of every record in context.
    '''
    template_name = "browse/browse_records.html"
    form_class = RecordForm

    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsView, self).get_form_kwargs()

        objs = self.filter_digobjects(self.request)
        pages = self.get_pages(objs)

        kwargs.update({
            'request': self.request,
            'pages': pages,
            'page': self.request.GET.get('page', 1)
        })
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsView, self).get_context_data(form, **kwargs)

        action_buttons = [
            ['review', 'Review Records'],
            ['add', 'Add Tags'],
            ['preview', 'Preview'],
            ['publish', 'Publish'],
            ['delete', 'Delete'],
        ]

        def title():
            # TODO: Title would be 'Select Records' when user arrives from the top level
            # "Search Results" when a result of a search.  Otherwise: "Review Records".
            return 'Select Records'

        # pager controls at top of form/table
        # this controls the checkbox selecting items across all pages
        pager_items_all = list(
            form.page.paginator.object_list.values_list('id', flat=True))

        context.update({
            'title': title(),
            'action_buttons': action_buttons,
            'form': form,
            'pager_items_all': pager_items_all,
            })
        return context

    def form_valid(self, form):

        objs = self.filter_digobjects(self.request)\
                   .values_list('pk', flat=True)\
                   .distinct()\
                   .filter(pk__in = self.request.session.get('items_submitted', None))

        self.request.session['objs'] = list(objs)

        action = self.request.POST.get('action', None)

        if action:
            if self.request.POST['action'] == 'add':
                return redirect('batch_add_tags')
            elif self.request.POST['action'] == 'review':
                return redirect('review_records')
            elif self.request.POST['action'] == 'delete':
                return redirect('delete_records')
            elif self.request.POST['action'] == 'connect':
                self.request.session['objs_active'] = list(
                    form.cleaned_data['items'])
                return redirect('connect')
            elif self.request.POST['action'] == 'connect_cancel':
                del self.request.session['objs_active']
                return redirect('connect')
            elif self.request.POST['action'] == 'preview':
                return redirect('preview_confirm')
            elif self.request.POST['action'] == 'publish':
                return redirect('publish_confirm')
            else:
                raise Http404
        else:
            raise Http404

class ReviewRecordsView(BrowseRecordsView):
    def get_form_kwargs(self):
        kwargs = super(ReviewRecordsView, self).get_form_kwargs()

        objs = self.filter_digobjects(self.request).filter(pk__in=
            self.request.session['objs'])
        pages = self.get_pages(objs)

        kwargs.update({
            'pages': pages
        })
        return kwargs

class BrowseRecordsByFacetValueView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular facet_value
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByFacetValueView, self).get_form_kwargs()

        self.facet_value = FacetValue.objects.get(pk=self.kwargs['pk'])
        dig_object_facet_values = DigObjectFacetValue\
            .objects.filter(facet_value = self.facet_value)
        objs = self.filter_digobjects(self.request)\
                   .filter(digobjectfacetvalue=dig_object_facet_values)
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByFacetValueView, self).get_context_data(form, **kwargs)

        def title():
            return 'Records Tagged "{}"'.format(self.facet_value)

        context.update({'title': title()})
        return context

class BrowseRecordsByFacetGroupView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular facet_group
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByFacetGroupView, self).get_form_kwargs()

        self.facet_group = FacetGroup.objects.get(pk=self.kwargs['pk'])

        objs = self.filter_digobjects(self.request)\
                   .filter(digobjectfacetvalue__facet_value__facet_group=self.facet_group)\
                   .distinct()
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByFacetGroupView, self)\
            .get_context_data(form, **kwargs)

        def title():
            return 'Records in the Facet Group "{}"'.format(self.facet_group)

        context.update({'title': title()})
        return context

class BrowseRecordsByValueView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular imported_value/value_element
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByValueView, self).get_form_kwargs()

        self.element = ValueElement.objects.get(pk=self.kwargs['pk'])
        values = ImportedValue.objects.filter(value=self.element)
        objs = self.filter_digobjects(self.request)\
                   .filter(importedvalue__in=values)
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByValueView, self).get_context_data(
            form, **kwargs)

        def title():
            return 'Records imported with the value "{}"'.format(self.element)

        context.update({'title': title()})
        return context

class BrowseRecordsByFormatView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular dcxml_format
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByFormatView, self).get_form_kwargs()

        self.format = DCXMLFormat.objects.get(pk=self.kwargs['pk'])
        #values = ImportedValue.objects.filter(dcxml_format=self.format)

        objs = self.filter_digobjects(self.request)\
                   .filter(importedvalue__dcxml_format=self.format)\
                   .distinct()
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByFormatView, self).get_context_data(form, **kwargs)

        def title():
            return 'Records imported with the field label "{}"'.format(self.format.field_label)

        context.update({'title': title()})
        return context


class BrowseValuesView(BrowseView):
    template_name = "browse/browse_values.html"
    form_class = SelectImportedValuesForm

    def get_form_kwargs(self):
        kwargs = super(BrowseValuesView, self).get_form_kwargs()
        objs = self.filter_digobjects(self.request)

        values = self.filter_values(self.request)

        if self.kwargs['pk']:
            # filter: to given field if the user has chosen one
            self.dcxml_format = DCXMLFormat.objects.get(pk=self.kwargs['pk'])
            values = values.filter(dcxml_format=self.dcxml_format)

        pages = self.get_pages(values)

        kwargs.update({'pages': pages,
                       'page': self.request.GET.get('page', 1)})
        return kwargs

    def get_context_data(self, form):
        context = super(BrowseValuesView, self).get_context_data(form)

        if self.dcxml_format:
            dcxml_field_name = self.dcxml_format.field_label
        else:
            dcxml_field_name = None

        action_buttons = [['review',  'Review Records'],
                          ['add',     'Add Tags'],
                          ['delete',  'Delete'],]

        pager_items_all = list(form.page.paginator.object_list.values_list('value', flat=True))

        context.update({
            'title': "Browse by Metadata: {}".format(dcxml_field_name),
            'subtitle': 'This view contains your original metadata.',
            'list_unit': 'value',
            'action_buttons': action_buttons,
            'form': form,
            'dcxml_field_name': dcxml_field_name,
            'pager_items_all': pager_items_all,
            })
        return context

    def form_valid(self, form):
        elements = self.request.session.get('items_submitted', None)
        values = self.filter_values(self.request).filter(value__in=elements)

        objs = self.filter_digobjects(self.request)\
                   .filter(pk__in=values.values_list("digobject", flat=True))\
                   .values_list('pk', flat=True).distinct()
        self.request.session['objs'] = list(objs)

        if form.cleaned_data['action'] == 'delete':
            return redirect('delete_records')
        elif form.cleaned_data['action'] == 'add':
            self.request.session['dcxml_format'] = self.dcxml_format.pk
            self.request.session['elements'] = elements
            return redirect('batch_add_tags')
        elif form.cleaned_data['action'] == 'review':
            return redirect('review_records')
        else:
            self.form_invalid(form)

#class BrowseRecordsViewByField(BrowseView):
#    template_name = "record/browse_records.html"
#    form_class = RecordForm
#    def get_form_kwargs(self):
#        objs = DigObject.objects.filter(pk__in=self.request.session['values'])
#        kwargs = super(ReviewRecordsView, self).get_form_kwargs()
#
#        field = DCXMLFormat.objects.get(pk=self.kwargs['pk'])
#        objs = DigObject.objects.filter(importedvalue__dcxml_format=field).distinct()
#
#        pages = Paginator(objs, self.request.GET.get('items_per_page', 20))
#        kwargs.update({'pages': pages, 'page': self.request.GET.get('page', 1)})
#        return kwargs
#
#    def form_valid(self, form):
#        self.request.session['objs'] = list(form.cleaned_data['items'])
#        if self.request.POST['action'] == 'add':
#            return redirect('batch_add_tags')
#        elif self.request.POST['action'] == 'delete':
#            return redirect('delete_records')
#        else:
#            raise Http404
#
#    def get_context_data(self, form):
#
#        field = DCXMLFormat.objects.get(pk=self.kwargs['pk'])
#
#        #context.update({
#        #    'title': 'Review Records',
#        #    'form': form,
#        #})
#        context.update({
#            'title': 'Browse Records by Field Name: {}'.format(field.field_label),
#            'form': form,
#        })
#        return context

class RecordView(DetailView):
    template_name = "record/record.html"
    model = DigObject

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)

        if 'title' in request.POST:
            title = request.POST.get('title', 0)
            self.object.title = title
            self.object.save()

        if 'reset_title' in request.POST:
            obj = self.object
            try:
                val = ImportedValue.objects.get(digobject=obj.pk, dcxml_format=1).value
            except:
                val = '<Untitled>'
            self.object.title = val.element
            self.object.save()

        cid = request.session['context']
        DigObjectContext.objects.get(digobject=self.object, context__pk=cid).set_dirty(True)

        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(RecordView, self).get_context_data(**kwargs)
        obj = context['object']
        cid = self.request.session['context']
        doc = DigObjectContext.objects.get(context=cid,
                                           digobject=obj)
        status = doc.digobject_status

        tags = obj.digobjectfacetvalue_set.filter(facet_value__facet_group__context=cid)

        parent_connections = Connection.objects.filter(child=obj)
        child_connections = Connection.objects.filter(parent=obj)

        # for display, to create a dict without repeating elements
        record_fields = {}
        for field in context['object'].importedvalue_set.all():
            key = field.dcxml_format.field_label
            val = field.value
            if not key in record_fields:
                record_fields[key] = [val]
            else:
                record_fields[key] += [val]

        if doc.context.preview_url and (status.status == 'Previewed' or status.status == 'Published'):
            preview_url = utils.get_obj_url(obj, doc.context.preview_url, "preview")
        else:
            preview_url = False

        if doc.context.publish_url and status.status == 'Published':
            publish_url = utils.get_obj_url(obj, doc.context.publish_url)
        else:
            publish_url = False

        context.update({
            'tab': 'metadata',
            'tags': tags,
            'parent_connections': parent_connections,
            'child_connections': child_connections,
            'status': status,
            'record_fields': sorted(record_fields.items()),
            'preview_url': preview_url,
            'publish_url': publish_url
            })
        return context

class UpdateRecordView(UpdateView):
    template_name = "record/update_record.html"
    model = DigObject
    form_class = FileFormSet

    def get_context_data(self, **kwargs):
        context = super(UpdateRecordView, self).get_context_data(**kwargs)
        obj = context['object']
        cid = self.request.session['context']
        status = DigObjectContext.objects.get(context=cid,
                                              digobject=obj).digobject_status
        path = utils.digobj_path(obj.id, cid).format('media') + 'thumbs/{}.jpg'.format(obj.ark)
        thumb = path.replace(settings.MEDIA_ROOT, settings.MEDIA_URL) if os.path.isfile(path) else False

        context.update({'tags': obj.digobjectfacetvalue_set.filter(facet_value__facet_group__context=cid),
                        'thumb': thumb,
                        'status': status})
        return context

    def form_valid(self, form):
        return redirect(self.get_object().get_absolute_url())

class DeleteRecordsView(FormView):
    template_name = 'record/delete_records.html'
    form_class = HiddenDigObjectsForm
    success_url = reverse_lazy('records_deleted')

    def get_initial(self):
        return {'objs': self.request.session.get('objs', [])}

    def get_context_data(self, **kwargs):
        context = super(DeleteRecordsView, self).get_context_data(**kwargs)
        context.update({
            'objs': DigObject.objects.filter(pk__in=self.request.session.get('objs', [])),
            'context': Context.objects.get(pk=self.request.session['context'])})
        return context

    def form_valid(self, form):
        self.request.session['goners'] = []
        self.request.session['survivors'] = []
        for obj in form.cleaned_data['objs']:
            obj.remove_from_context(self.request.session['context'])
            if obj.digobjectcontext_set.count() == 0:
                self.request.session['goners'].append(obj.pk)
            else:
                self.request.session['survivors'].append(obj.pk)
        delete_objects.delay(self.request.session['goners'])
        return redirect(self.get_success_url())

class DeleteRecordView(DeleteView):
    model = DigObject
    template_name = "record/delete_record.html"
    success_url = reverse_lazy('records_deleted')

    def get_context_data(self, **kwargs):
        context = super(DeleteRecordView, self).get_context_data(**kwargs)
        context.update({
            'context': Context.objects.get(pk=self.request.session['context'])
        })
        return context

    def delete(self, request, *args, **kwargs):
        self.request.session['goners'] = []
        self.request.session['survivors'] = []
        self.object = self.get_object()
        self.object.remove_from_context(self.request.session['context'])
        if self.object.digobjectcontext_set.count() == 0:
            self.request.session['goners'].append(self.object.pk)
        else:
            self.request.session['survivors'].append(self.object.pk)
        delete_objects.delay(self.request.session['goners'])
        return redirect(self.get_success_url())

class RecordsDeletedView(TemplateView):
    template_name = "record/records_deleted.html"

    def get_context_data(self, **kwargs):
        context = super(RecordsDeletedView, self).get_context_data(**kwargs)
        context.update({'goners': DigObject.objects.filter(pk__in=self.request.session.get('goners', [])),
                        'survivors': DigObject.objects.filter(pk__in=self.request.session.get('survivors', [])),
                        'context': Context.objects.get(pk=self.request.session['context'])})
        return context

class DCXMLView(DetailView):
    model = DigObject

    def get_mimetype(self):
        return 'application/xml'

    def get(self, *args, **kwargs):
        digobj = self.get_object()
        full_path = processor.generate_dc_xml(digobj.pk, self.request.session['context'])

        filename = "{}.dc.xml".format(digobj.ark)

        if os.path.isfile(full_path):
            f = open(full_path, 'rb')
            response = HttpResponse(f.read(), mimetype=self.get_mimetype())
            f.close()
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
            response['Content-Length'] = os.path.getsize(full_path)
            return response
        else:
            raise Http404

class OriginalMetadataView(DetailView):
    model = DigObject

    def get_mimetype(self):
        return 'application/xml'

    def get(self, *args, **kwargs):
        digobj = self.get_object()
        context = Context.objects.get(pk=self.request.session['context'])

        path = utils.digobj_path(digobj.pk, context.pk)
        filename    = digobj.ark + '.xml'
        full_path   = (path + filename).format('data')

        if os.path.isfile(full_path):
            f = open(full_path, 'rb')
            response = HttpResponse(f.read(), mimetype=self.get_mimetype())
            f.close()
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
            response['Content-Length'] = os.path.getsize(full_path)
            return response
        else:
            raise Http404
