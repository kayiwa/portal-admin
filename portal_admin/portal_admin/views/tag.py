from django.views.generic import DeleteView, FormView, UpdateView
from portal_admin.views.browse import FilterDigObjectsMixin, BrowseRecordsView

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.core.urlresolvers import reverse, resolve
from urllib.parse import urlsplit

from portal_admin.models import (Context,
                                 DCXMLFormat,
                                 DigObject,
                                 DigObjectContext,
                                 DigObjectFacetValue,
                                 FacetGroup,
                                 FacetValue,
                                 ImportedValue,
                                 MetaEvent,
                                 ValueElement,)

from portal_admin.forms.tag import (FacetGroupFormSet,
                                    FacetValuesForm,
                                    RetagForm,
                                    TagFilterForm,)
from portal_admin.forms.record import DigObjectFilterForm

from django.db.models import Count

from portal_admin import autotag

#
# Landing page
#

class TagView(BrowseRecordsView):
    template_name = "browse/browse_tags.html"
    def get_context_data(self, **kwargs):
        context = super(TagView, self).get_context_data(**kwargs)

        action_buttons = [['add', 'Add Tags'],]

        context.update({'title': 'Tag',
                        'action_buttons': action_buttons,})
        return context

class BatchAddTagsView(FormView):
    '''
    Add several tags to DigObject(s) at once.
    Each tab represents a FacetGroup, using FormSet for the form.
    '''
    template_name = "tag/add_tags.html"
    form_class = FacetGroupFormSet

    def get_referer_view(self):
        '''A utility method for identifying whether this View was requested by the Browse by Metadata View.'''
        try:
            referer_url = self.request.META['HTTP_REFERER']
            referer_path = urlsplit(referer_url)[2]
            referer_view = resolve(referer_path).url_name

        except KeyError:
            referer_view = None

        return referer_view

    def get_form_kwargs(self):
        kwargs = super(BatchAddTagsView, self).get_form_kwargs()

        # filter: facet groups associated with this context
        groups = FacetGroup.objects.filter(context=self.request.session['context'])

        kwargs.update({'groups': groups,})
        return kwargs

    def get_context_data(self, form):
        context = super(BatchAddTagsView, self).get_context_data()
        objs_from_session = DigObject.objects\
                                     .filter(pk__in=self.request.session.get('objs', []))

        # only use 'elements' if coming from browse values
        if self.get_referer_view() == 'browse_values_2':
            elements = ValueElement.objects\
                                   .filter(pk__in=self.request.session['elements'])
            dcxml_format = DCXMLFormat.objects\
                                      .get(pk=self.request.session['dcxml_format'])

            # create nested list for template
            objs_nested = {}
            for element in elements:
                values = ImportedValue.objects.filter(value=element)
                objs = DigObject.objects\
                                .filter(importedvalue__in=values)\
                                .filter(pk__in=objs_from_session)
                objs_nested[element] = objs

            context.update({'dcxml_format': dcxml_format,
                            'objs_nested': objs_nested})
        elif 'elements' in self.request.session:
            del self.request.session['elements']

        context.update({'objs_flat': objs_from_session,
                        'form': form,})
        return context

    def form_valid(self, formset):
        # collect all the tags selected in any form of the formset
        tags = [v for f in formset for v in f.cleaned_data.get('values', [])]

        # get all the digobjs saved from the last step
        values = self.request.session.get('objs', [])
        cid = self.request.session['context']

        # for each tag/obj combo make a new facet connection
        meta_event = MetaEvent.objects.create(context=Context.objects.get(pk=cid),
                                              user=self.request.user)
        objs = DigObjectContext.objects.filter(pk__in=self.request.session.get('objs', []), context__pk=cid)
        for tag in tags:
            for obj in objs:
                fv, created = DigObjectFacetValue.objects.get_or_create(digobject=obj.digobject, facet_value=tag)
                if created:
                    obj.set_dirty(True)
                    meta_event.apply_tag_event(obj.digobject)

        return render_to_response('tag/tags_added.html',
                                  {'tags': tags,
                                   'objs': objs},
                                  RequestContext(self.request))

class AddTagsView(UpdateView):
    template_name = "tag/add_tags.html"
    form_class = FacetGroupFormSet
    model = DigObject

    def get_form_kwargs(self):
        kwargs = super(AddTagsView, self).get_form_kwargs()

        # filter: facet groups associated with this context
        groups = FacetGroup.objects.filter(
            context=self.request.session['context'])

        kwargs.update({'groups': groups,})

        return kwargs

    def form_valid(self, formset):
        # collect all the tags selected in any form of the formset
        tags = [v for f in formset for v in f.cleaned_data.get('values', [])]
        obj = self.get_object()
        cid = self.request.session['context']
        meta_event = MetaEvent.objects.create(context=Context.objects.get(pk=cid),
                                              user=self.request.user)
        dc = DigObjectContext.objects.get(digobject=obj, context__pk=cid)

        for tag in tags:
            fv, created = DigObjectFacetValue.objects.get_or_create(digobject=obj, facet_value=tag)
            if created:
                dc.set_dirty(True)
                meta_event.apply_tag_event(obj)

        return redirect(self.get_success_url())

    def get_success_url(self):
        return self.request.GET.get("next", reverse('record', kwargs={'pk': self.get_object().pk}))



#
# Browse by tags
#

class FacetValuesView(FilterDigObjectsMixin, FormView):
    template_name = "tag/facet_values.html"
    form_class = FacetValuesForm

    def get_form_kwargs(self):
        kwargs = super(FacetValuesView, self).get_form_kwargs()
        group = get_object_or_404(FacetGroup, pk=self.kwargs.get('pk'))
        objs = self.filter_digobjects(self.request)

        # filter: facet values in this group that are attached to objects in this institution/context
        # aggregate: count of digobjects attached to this facet value
        tf = TagFilterForm(self.request.GET)
        if tf.is_valid():
            facet_counts = DigObjectFacetValue.objects\
                                              .filter(facet_value__facet_group=group,
                                                      digobject__in=objs,
                                                      is_autoassigned=tf.cleaned_data['assigned']=='True')\
                                              .values('facet_value')\
                                              .annotate(count=Count('pk'))
        else:
            facet_counts = DigObjectFacetValue.objects\
                                              .filter(facet_value__facet_group=group,
                                                      digobject__in=objs)\
                                              .values('facet_value')\
                                              .annotate(count=Count('pk'))
        counts = {}
        for f in facet_counts:
            counts[f['facet_value']] = f['count']

        facet_values = FacetValue.objects.filter(pk__in=counts.keys()).order_by('value')

        kwargs.update({'values': facet_values, 'counts': counts})
        return kwargs

    def get_context_data(self, form):
        context = super(FacetValuesView, self).get_context_data()
        result_count = len(self.get_form_kwargs()['values'])

        context.update({'group': get_object_or_404(FacetGroup, pk=self.kwargs.get('pk')),
                        'filter_form': DigObjectFilterForm(self.request.GET),
                        'tag_form': TagFilterForm(self.request.GET),
                        'form': form,
                        'result_count': result_count})
        return context

    def form_valid(self, form):
        values = form.cleaned_data['values']
        objs = self.filter_digobjects(self.request)
        # filter: all digobjectfacetvalues with given facet values and in selected objects
        facets = DigObjectFacetValue.objects.filter(digobject__in=objs, facet_value__in=values)
        if form.cleaned_data['action'] == 'delete':
            # do the delete
            cid = self.request.session['context']
            meta_event = MetaEvent.objects.create(context=Context.objects.get(pk=cid),
                                                  user=self.request.user)

            for d in DigObjectContext.objects.filter(context__pk=cid, digobject__in=facets.values_list('digobject', flat=True)):
                d.set_dirty(True)

            for f in facets:
                meta_event.delete_tag_event(f.digobject)
            facets.delete()

            return redirect('facet_values', self.kwargs.get('pk'))
        elif form.cleaned_data['action'] == 'review':

            objs = self.filter_digobjects(self.request)\
                       .values_list('pk', flat=True)\
                       .filter(digobjectfacetvalue__in=facets).distinct()

            self.request.session['objs'] = list(objs)

            return redirect('review_records')
        else:
            self.form_invalid(form)

#class ReviewTagsView(FormView):
#    template_name = "tag/review_tags.html"
#    form_class = DigObjectFacetValueForm
#
#    def get_form_kwargs(self):
#        kwargs = super(ReviewTagsView, self).get_form_kwargs()
#        # filter: digobjectfacet values from last step
#        pages = Paginator(
#            DigObjectFacetValue.objects.filter(
#                pk__in=self.request.session['values']),
#            self.request.GET.get('items_per_page', 20))
#        kwargs.update({'pages': pages, 'page': self.request.GET.get('page', 1)})
#        return kwargs
#
#    def form_valid(self, form):
#        DigObjectFacetValue.objects.filter(pk__in=form.cleaned_data['values']).delete()
#        # return to the main view
#        # TODO: we could use a message to report number of values deleted
#        return self.render_to_response(self.get_context_data(form=self.get_form(self.get_form_class())))

class RetagView(FormView):
    template_name = "tag/retag.html"
    form_class = RetagForm

    def form_valid(self, form):
        meta_event = MetaEvent.create(context=Context.objects.get(pk=self.request.session['context']),
                                      user=self.request.user)
        for i in form.cleaned_data['institutions']:
            objs = DigObjectContext.objects.filter(digobject__profile__institution=i,
                                                   context=context).values_list('digobject').distinct()
            autotag.autotag(objs, form.cleaned_data['trigger_terms'], meta_event)
        return render_to_response('tag/retag_confirmation.html',
                                  {'institutions': form.cleaned_data['institutions'],
                                   'trigger_terms': form.cleaned_data['trigger_terms']},
                                  RequestContext(self.request))

class DeleteTagView(DeleteView):
    model = DigObjectFacetValue

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.digobject = self.get_object().digobject
        cid = self.request.session['context']
        meta_event = MetaEvent.objects.create(context=Context.objects.get(pk=cid),
                                              user=self.request.user)
        DigObjectContext.objects.get(digobject=self.digobject, context=cid).set_dirty(True)
        meta_event.delete_tag_event(self.digobject)
        result =  super(DeleteTagView, self).delete(request, *args, **kwargs)
        return result

    def get_success_url(self):
        return self.request.GET.get("next", reverse('record', kwargs={'pk': self.digobject.pk}))
