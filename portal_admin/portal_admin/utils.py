import urllib
import portal_admin.settings as portal_admin_settings
# small utilities used in various ways throughout the system

# ~~~ generate ARK identifier ~~~~~~~~~~~~~~~~~~~~~~~~~~~|
def ezid_request(url, method, data=None):
    """
    Sends ark requests to EZID
    """
    from django.conf import settings

    request = urllib.request.Request(url)
    if data:
        request.add_header("Content-Type", "text/plain; charset=UTF-8")
        request.add_data(data.encode("UTF-8"))
    request.get_method = lambda: method
    h = urllib.request.HTTPBasicAuthHandler()
    h.add_password("EZID", settings.EZID_URL, settings.EZID_USERNAME, settings.EZID_PASSWORD)
    opener = urllib.request.build_opener(h)
    connection = opener.open(request)
    response = connection.read()
    return response.decode("UTF-8")

def generate_ark():
    """
    Generates a unique identifier.
    """
    from django.conf import settings
    
    if settings.USE_EZID:
        return ezid_request(settings.EZID_MINT_URL, "POST").split(settings.EZID_ARK)[1]
    else:
        import uuid
        return str(uuid.uuid1())

def update_ark_url(ark, url):
    """
    Updates the url associated with an ark
    """
    from django.conf import settings

    if settings.USE_EZID:
        return ezid_request((settings.EZID_ARK_URL + ark), "POST", data=("_target: {}".format(url)))

def update_ark_status(ark, status):
    from django.conf import settings

    if settings.USE_EZID:
        return ezid_request((settings.EZID_ARK_URL + ark), "POST", data=("_status: {}".format(status)))

def get_obj_url(obj, context_url, suffix=""):
    from urllib.parse import urlparse

    # This is bad.  we're assuming the format of the links 
    # but we're going to do it for preview release
    # FIXME later EV 7/30/2015
    if obj.digobject_form._type == 'images':
        f = 'image'
    elif obj.digobject_form._format == 'PDF':
        f = 'marcxml'
    else:
        f = obj.digobject_form._format.lower()

    p = urlparse(context_url)
    return "{}://{}/{}/{}/{}/{}/{}".format(p.scheme,
                                           p.netloc,
                                           f,
                                           obj.profile.institution.xtf_name,
                                           obj.profile.pk,
                                           obj.ark,
                                           suffix)

# ~~~ Cleanup trailing characters ~~~~~~~~~~~~~~~~~~~~~~~|
def trailing_cleanup(v):
    """
    A customized cleanup routine, created because many of the
    input XML tends to have input of the form

    <author>Mark O'Malley,</author>

    This routine will trim any characters found in
    settings.TRAILING_CLEANUP from the last character of
    a given string.  ie: will return "Mark O'Malley"
    """
    from django.conf import settings

    if not v or len(v) == 0:
        return v

    while v[-1] in settings.TRAILING_CLEANUP:
        v = v[:-1]
    return v

# ~~~ get value from XML element/record ~~~~~~~~~~~~~~~~~|
class FieldNotFound(Exception):
    pass

class ValNotFound(Exception):
    pass

def get_clean_values(record, xpath, instance, delim=None):
    '''
    Remove root from xpath, return value found at relative path.

    Note: The system does not store a root node, but rather assumes
    that the archivist enters a full xpath.  At this point in the processing.
    however, the record is a parsed XML record and all xpaths it can follow
    are relative to the root; rather than inclusive of it.
    '''

    path_elements = xpath.split("/")
    if path_elements[-1].startswith('@'):
        attribute = path_elements[-1][1:]
        path_elements.pop()
    else:
        attribute = None
    xpath = "/".join(path_elements[1:])

    try:
        vals = [v for v in record.findall(xpath)]
        if attribute:
            vals = [v.attrib[attribute] for v in vals]
        else:
            vals = [v.text for v in vals]
        if delim:
            vals = [v.split(delim) for v in vals]
            vals = [v for vi in vals for v in vi]
        if len(vals) == 0:
            raise FieldNotFound
    except AttributeError:
        raise FieldNotFound
    except KeyError:
        raise FieldNotFound

    vals = [v.strip() for v in vals if v]
    if instance == None:
        instance = 0
    # if there are no values or there is no value in the selected slot
    # we didn't find a value otherwise just ignore blanks
    if len(vals) < 1 or (instance > -1 and len(vals) < (instance + 1)):
        raise ValNotFound

    vals = [trailing_cleanup(v) for v in vals]

    if instance == -1:
        for val in vals:
            yield val
    else:
        yield vals[instance]

# ~~~ Assembles a value, given certain ElementSources ~~|
def assemble_value(record, profile_id, target):
    """
    Given a multi-record value, such as the title of a record,
    uses the combination of target string, and concat_order, to
    re-assemble a full string from potentially many records.
    """

    from portal_admin.models import Profile, ElementSource

    profile = Profile.objects.get(id=profile_id)
    sources = ElementSource.objects.filter(profile=profile, dcxml_format__element_name=target)\
                                   .order_by('dcxml_format__concat_order')

    val = ''
    for s in sources:
        try:
            vals = [v for v in get_clean_values(record.record, s.xpath, 0)]
        except FieldNotFound:
            continue
        except ValNotFound:
            continue
        assert len(vals) == 1
        _val = vals[0]
        if _val.endswith(",") or _val.endswith(";"):  # Custom Cleanup
            _val = _val[:-1]
        if s.dcxml_format.separator:
           val += s.dcxml_format.separator+" "
        val += _val
    return val[:255]

# ~~~ checksum of a file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
def get_checksum(content):
    """
    Given a content string, returns the checksum of the string.
    Currently using python's hashlib.md5()
    """

    import hashlib
    m = hashlib.md5()
    m.update(content)
    return str(m.digest())

# ~~~ content pointer ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
def get_content_pointer(record, f):
    """
    Given a record, and a FileSource, returns
    the value pointed to by the FileSource - either
    a URI or a path on disk.
    """

    if f.use_filename:
        val = record.filename.replace(".xml", "")
    else:
        vals = [v for v in get_clean_values(record.record, f.xpath, f.instance_number if f.instance_number else 0)]
        assert len(vals) == 1
        val = vals[0]

        if f.profile.is_remote:
            return val

        if f.has_extension:
            val = ".".join(val.split(".")[:-1])

    if f.suffix and len(f.suffix):
        return val + f.suffix

    return val

# ~~~ split one large, mutli-record xml ~~~~~~~~~~~~~~~~|
def split_xml(fpath, profile_id, dest_path):
    """
    Given an XML file, pointed to by fpath; a profile which
    indicates the split_node, and a destination;
    creates a set of individual XML files, one per record,
    as defined by the original XML file.
    Only used in the case of multi-record XML uploads.
    """

    from portal_admin.models import Profile
    from xml.etree.ElementTree import parse, Element, ElementTree

    profile = Profile.objects.get(id=profile_id)

    assert profile.split_node
    assert len(profile.split_node)

    with open(fpath,'r') as f:
        records = parse(f).findall(profile.split_node)

    assert len(records) > 0

    assert "/" in fpath
    fname = fpath.split("/")[-1] # /path/to/file.xml -> "file.xml"
    index = fname.rfind(".")
    base_name = fname[:index] if index > -1 else fname  # /path/to/file.xml -> "file"

    for i, record in enumerate(records):
        dest_fname = dest_path+'/'+ base_name + "_" + str(i)+".xml"
        dest_file = open(dest_fname, 'wb')
        ElementTree(record).write(dest_file)
        dest_file.close()

# ~~~ finds a file, returns full path ~~~~~~~~~~~~~~~~~~~|
def find_fpath(pattern, _type, fdir):
    """
    Given that XML records may only describe a filename - not
    its extension, this routine finds any file of the specified pattern
    whose extension is one of the ALLOWED_EXTS[_type] in question.
    """

    import os
    from django.conf import settings

    files = os.listdir(fdir)
    files = [f for f in files if f.rfind(".") > -1 and f[:f.rindex(".")] == pattern]
    files = [f for f in files if f.split(".")[-1] in settings.ALLOWED_EXTS[_type]]

    try:
        assert len(files) == 1
    except:
        return None
    return fdir + files[0]

# ~~~ downloads a uri to cwd(), returns full fpath ~~~~~|
class WgetExtError(Exception):
    pass
class WgetHttpError(Exception):
    pass
class WgetNoContent(Exception):
    pass

def wget_content(uri, _type, dest_dir):
    '''
    Given a URI, uses python-wget to attempt to retrieve content and
    verify it.  Raises a set of descriptive errors, per known case.
    '''

    import os
    import wget
    from django.conf import settings

    curdir = os.getcwd()

    ext = uri.split(".")[-1]
    if ext not in settings.ALLOWED_EXTS[_type]:
        raise WgetExtError

    if not uri.startswith('http'):
        raise WgetHttpError

    try:
        os.chdir(dest_dir)
        fname = wget.download(uri, False)
    except ValueError:
        raise WgetNoContent
    except urllib.error.HTTPError:
        raise WgetNoContent
    finally:
        os.chdir(curdir)

    return dest_dir + fname

# ~~~ snapshot the database, create fixture ~~~~~~~~~~~~|
def snapshot_fixtures(outfile):
    """
    Takes a snapshot of known fixtures, useful for debugging during testing.
    """

    from io import StringIO
    from django.core.management import call_command

    content = StringIO()
    call_command('dumpdata', 'portal_admin', verbosity=0, stdout=content)
    content.seek(0)
    data = content.read()
    outfile = open(outfile,'w')
    outfile.write(data)
    outfile.close()

# ~~~ digobj.path, given context ~~~~~~~~~~~~~~~~~~~~~~~|

def digobj_path(digobject_id, context_id, status_id=None):
    """
    Returns a tuple, describing the file-structure for files related to this DigObject.  Supplying a status (usually "Deposited") will return a path to the files in that status branch; otherwise, will return the path for the object's current status branch.
    """

    from portal_admin.models import Context, DigObject, DigObjectContext, DigObjectStatus
    from django.conf import settings

    digobj = DigObject.objects.get(id=digobject_id)
    context = Context.objects.get(id=context_id)
    digobj_context = DigObjectContext.objects.get(digobject=digobj, context=context)
    if status_id:
        status = DigObjectStatus.objects.get(pk=status_id)
    else:
        status = digobj_context.digobject_status

    return (settings.FILE_ROOT + status.status + '/{}/' + "{}/{}/{}/".format(digobj.digobject_form._type,
                                                                             digobj.profile.institution.xtf_name,
                                                                             digobj.profile.id,))


def logfile_path(inst, profile_id):
    from django.conf import settings
    import datetime, os

    logroot = "{}/logs/{}/{}/".format(settings.FILE_ROOT, inst, profile_id)
    os.makedirs(logroot, exist_ok=True)

    # if there was output to standard err or we didn't find success messages
    # for all the objects we make an error log
    log_path = "logs/{}/{}/{}_index.txt".format(inst,
                                             profile_id,
                                             datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
    while os.path.exists(settings.FILE_ROOT + log_path):
        log_path = "logs/{}/{}/{}_index.txt".format(inst,
                                                    profile_id,
                                                    datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
        time.sleep(1)
    return log_path

# ~~~ ensures the file structure(s) required exist ~~~~~|
def ensure_dirs(path):
    """
    Ensures that the directory structure described by "path" exists,
    creates it if it does not.
    """

    import os

    os.makedirs(path.format('data'), exist_ok=True)
    os.makedirs(path.format('media'), exist_ok=True)
    os.makedirs(path.format('media') + "thumbs/", exist_ok=True)

def location_path(xtf_name):
    from django.conf import settings
    import os

    path = "{}brand/{}/locations/".format(settings.FILE_ROOT, xtf_name)
    os.makedirs(path, exist_ok=True)
    return path

def brand_path(xtf_name):
    from django.conf import settings
    import os

    path = "{}brand/{}/".format(settings.FILE_ROOT, xtf_name)
    os.makedirs(path, exist_ok=True)
    return path

def ensure_symlinks(digobject_id, context_id, status_name):
    """
    Ensures that symlinks for an object exist in its current (Status-based) file tree;
    creates them if not.  All symlinks point back to the original ("Deposited") status tree.
    """
    from portal_admin.models import DigObject, DigObjectStatus
    import os, glob

    deposited = DigObjectStatus.objects.get(status="Deposited")
    target_status = DigObjectStatus.objects.get(status=status_name)

    target_path = digobj_path(digobject_id, context_id, target_status.id)
    deposited_path = digobj_path(digobject_id, context_id, deposited.id)

    obj = DigObject.objects.get(pk=digobject_id)

    ensure_dirs(target_path)

    paths = {deposited_path.format('data'): target_path.format('data'),
             deposited_path.format('media'): target_path.format('media'),
             (deposited_path.format('media') + 'thumbs/'): (target_path.format('media') +'thumbs/')}

    for p1, p2 in paths.items():
        files = glob.glob("{}{}*".format(p1, obj.ark))
        for f in files:
            basename = os.path.basename(f) 
            link = "{}{}".format(p2, basename)
            if not os.path.isfile(link):
                os.symlink(f, link)
            else:
                os.utime(f, None)

def make_thumbnail(src, dest, size=portal_admin_settings.THUMBNAIL_SIZE):
    from django.conf import settings
    make_downsample_image(src, dest, size)

def make_main_image(src, dest, size=portal_admin_settings.IMAGE_SIZE_MAIN):
    from django.conf import settings
    make_downsample_image(src, dest, size)

def make_downsample_image(src, dest, size_):
    '''
    Resize an image down to a size, but no smaller.  Orientation agnostic.
    '''
    from PIL import Image

    with open(src, 'rb') as src_f:
        i_source = Image.open(src_f)
        ratio = i_source.size[0] / i_source.size[1]

        if ratio >= 1: #landscape or square
            target_size = (max(size_), min(size_))
        else: #portrait
            target_size = (min(size_), max(size_))# flip

        # either dimension smaller than settings?  leave alone.
        if i_source.size[0] <= target_size[0] or i_source.size[1] <= target_size[1]:
            i_dest = i_source

        # else: resize, using settings as *minimums*
        else:
            if ratio >= 1: #landscape or square
                w = target_size[0]
                h = int(target_size[0] / ratio)
                if h < target_size[1]:
                    w = int(target_size[1] * ratio)
                    h = target_size[1]
            else: # portrait
                w = int(target_size[1] * ratio)
                h = target_size[1]
                if w < target_size[0]:
                    w = target_size[0]
                    h = int(target_size[0] / ratio)

            new_size = (w, h)
            i_dest = i_source.resize(new_size, Image.LANCZOS)

        # save out
        i_dest.save(dest, "JPEG")

# ~~~ remove any namespace declarations from XML ~~~~~~~|
def strip_ns(xml_string):
    """
    Given an string representing an XML structure, remove
    any namespace-related information.  Return the cleaned
    string.
    """

    import re
    matches = re.findall(' xmlns:\S+"', xml_string)
    ns_tags = [m.split("xmlns:")[1].split("=")[0] for m in matches]
    xml_string = re.sub(' xmlns\S+"', '', xml_string)
    xml_string = re.sub(' xsi:schemaLocation="[^"<>]+"', '', xml_string)
    for tag in ns_tags:
        xml_string = re.sub(tag+':', '', xml_string)
    return xml_string


