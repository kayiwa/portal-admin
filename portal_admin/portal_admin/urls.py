from django.conf.urls import patterns, include, url
from django.conf import settings

from django.conf.urls.static import static
from django.contrib.staticfiles import views

from portal_admin.decorators import session_required
from django.contrib.auth.decorators import login_required

from django.contrib import admin
admin.autodiscover()

from portal_admin.views.auth import *
from portal_admin.views.admin import *
from portal_admin.views.deposit import *
from portal_admin.views.tag import *
from portal_admin.views.browse import *
from portal_admin.views.connect import *
from portal_admin.views.publish import *
from portal_admin.views.preview import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'portal_admin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$',
        session_required(OverviewView.as_view()),
        name="overview"),

    url(r'^help/$',
        session_required(HelpView.as_view()),
        name="help"),
    url(r'^help/form/$',
        session_required(HelpFormView.as_view()),
        name="help_form"),
    url(r'^help/sent/$',
        session_required(HelpSentView.as_view()),
        name="help_sent"),

    url(r'^admin/logs/$',
        session_required(ErrorLogsView.as_view()),
        name="error_logs"),
    url(r'^admin/logs/(?P<pk>\d+)/$',
        session_required(ErrorLogDownloadView.as_view()),
        name='error_log'),
    url(r'^admin/logs/(?P<pk>\d+)/delete/$',
        session_required(DeleteErrorLogView.as_view()),
        name="delete_error_log"),

    url(r'^admin/account/password/$',
        'django.contrib.auth.views.password_change',
        {'template_name': 'admin/change_password.html'},
        name="password_change"),
    url(r'^admin/account/password/changed/$',
        'django.contrib.auth.views.password_change_done',
        {'template_name': 'admin/password_changed.html'},
        name="password_change_done"),

    # password reset
    url(r'^accounts/password/reset/$', 'django.contrib.auth.views.password_reset',
        {'post_reset_redirect' : '/accounts/password/reset/done/'}),
    url(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
    url(r'^accounts/password/reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    url(r'^accounts/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect' : '/accounts/password/done/'}),
    url(r'^accounts/password/done/$', 'django.contrib.auth.views.password_reset_complete', 
        name='password_reset_complete'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/login/$',
        LoginView.as_view(),
        name="login"),
    url(r'^accounts/selectcontext/$',
        login_required(SelectContextView.as_view()),
        name="select_context"),
    url(r'^accounts/logout/$',
        'django.contrib.auth.views.logout_then_login',
        name="logout"),
    url(r'^overview/$',
        session_required(OverviewView.as_view()),
        name="overview"),

    url(r'^deposit/$',
        session_required(DepositView.as_view()),
        name="deposit"),
    url(r'^deposit/profile/add/(?P<pk>\d+)/$',
        session_required(CreateProfileWizard.as_view()),
        name="create_profile"),
    url(r'^deposit/profile/(?P<pk>\d+)/saved/$',
        session_required(ProfileSavedView.as_view()),
        name="profile_saved"),
    url(r'^deposit/profile/(?P<pk>\d+)/upload/$',
        session_required(UploadWizard.as_view()),
        name="upload"),
    url(r'^deposit/profile/(?P<pk>\d+)/uploaded/$',
        session_required(UploadedView.as_view()),
        name="uploaded"),
    url(r'^deposit/profile/(?P<pk>\d+)/$',
        session_required(UploadRulesView.as_view()),
        name="upload_rules"),

    url(r'^validatexpath/',
        'portal_admin.views.deposit.validate_xpath',
        name='validate_xpath'),
    url(r'^validateseparator/',
        'portal_admin.views.deposit.validate_separator',
        name='validate_separator'),
    url(r'^validatefilesource/',
        'portal_admin.views.deposit.validate_filesource',
        name='validate_filesource'),

    url(r'^browse/$', session_required(BrowseOverView.as_view()),
        name="browse"),

    url(r'^tag/$',
        session_required(TagView.as_view()),
        name="tag"),
    url(r'^tag/(?P<pk>\d+)/$',
        session_required(AddTagsView.as_view()),
        name="add_tags"),
    url(r'^tag/choose/$',
        session_required(BatchAddTagsView.as_view()),
        name="batch_add_tags"),
    url(r'^tag/facetvalues/(?P<pk>\d+)/$',
        session_required(FacetValuesView.as_view()),
        name="facet_values"),
    #url(r'^tag/review/$',
    #    session_required(ReviewTagsView.as_view()),
    #    name="review_tags"),
    url(r'^tag/retag/$',
        session_required(RetagView.as_view()),
        name="retag"),
    url(r'^tag/delete/(?P<pk>\d+)/$',
        session_required(DeleteTagView.as_view()),
        name="delete_tag"),

    # TODO combine the following two urls; how to pass parameter via DCXMLFormat.get_absolute_url() ?
    url(r'^values/$',
        session_required(BrowseValuesView.as_view()),
        name="browse_values"), # from search/browse
    url(r'^values/(?P<pk>\d+)/$',
        session_required(BrowseValuesView.as_view()),
        name="browse_values_2"), # from tag
    url(r'^records/$',
        session_required(BrowseRecordsView.as_view()),
        name="browse_records"),
    url(r'^records/formats/(?P<pk>\d+)/$',
        session_required(BrowseRecordsByFormatView.as_view()),
        name="browse_records_by_format"),
    url(r'^records/facet_values/(?P<pk>\d+)/$',
        session_required(BrowseRecordsByFacetValueView.as_view()),
        name="browse_records_by_facet_value"),
    url(r'^records/facet_group/(?P<pk>\d+)/$',
        session_required(BrowseRecordsByFacetGroupView.as_view()),
        name="browse_records_by_facet_group"),
    url(r'^records/value/(?P<pk>\d+)/$',
        session_required(BrowseRecordsByValueView.as_view()),
        name="browse_records_by_value"),
    url(r'^records/review/$',
        session_required(ReviewRecordsView.as_view()),
        name="review_records"),
    url(r'^records/(?P<pk>\d+)/$',
        session_required(RecordView.as_view()),
        name="record"),
    url(r'^records/(?P<pk>\d+)/delete/$',
        session_required(DeleteRecordView.as_view()),
        name='delete_record'),
    url(r'^records/(?P<pk>\d+)/update/$',
        session_required(UpdateRecordView.as_view()),
        name='update_record'),
    url(r'^records/(?P<pk>\d+).xml$',
        session_required(OriginalMetadataView.as_view()),
        name='record_original_metadata'),
    url(r'^records/(?P<pk>\d+).dc.xml$',
        session_required(DCXMLView.as_view()),
        name='record_dcxml'),
    url(r'^records/delete/',
        session_required(DeleteRecordsView.as_view()),
        name='delete_records'),
    url(r'^records/deleted/',
        session_required(RecordsDeletedView.as_view()),
        name='records_deleted'),

    url(r'^connect/$',
        session_required(ConnectView.as_view()),
        name='connect'),
    url(r'^connect/confirm/',
        session_required(ConnectConfirmView.as_view()),
        name='connect_confirm'),
    url(r'^connect/(?P<pk>\d+)/delete/',
        session_required(ConnectDeleteView.as_view()),
        name='connect_delete'),

    url(r'^preview/$',
        session_required(PreviewView.as_view()),
        name='preview'),
    url(r'^preview/(?P<pk>\d+)/$',
        session_required(PreviewRecordView.as_view()),
        name='preview_record'),
    url(r'^preview/confirm/',
        session_required(PreviewConfirmView.as_view()),
        name='preview_confirm'),

    url(r'^publish/$',
        session_required(PublishView.as_view()),
        name="publish"),
    url(r'^publish/(?P<pk>\d+)/$',
        session_required(PublishRecordView.as_view()),
        name='publish_record'),
    url(r'^publish/confirm/',
        session_required(PublishConfirmView.as_view()),
        name='publish_confirm'),

    url(r'^reindex/$',
        session_required(ReindexConfirmView.as_view()),
        name='reindex'),

    url(r'^upload/$', upload, name='jfu_upload'),
    url(r'^upload/delete/$', upload_delete, name='jfu_delete'),
)

#if not settings.PRODUCTION_SERVER:
#    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#    urlpatterns += [
#        url(r'^static/(?P<path>.*)$', views.serve),
#    ]
